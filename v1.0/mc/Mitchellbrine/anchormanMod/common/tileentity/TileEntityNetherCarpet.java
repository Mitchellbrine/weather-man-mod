package mc.Mitchellbrine.anchormanMod.common.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityNetherCarpet extends TileEntity {

	public static int state;
	
	
	public void readFromNBT(NBTTagCompound nbt) {
		super.readFromNBT(nbt);
		if (!nbt.hasKey("state")) {
			state = 0;
		}
		else {
			nbt.getInteger("state");
		}
	}
	
	public void writeToNBT(NBTTagCompound nbt) {
		super.writeToNBT(nbt);
		nbt.setInteger("state", state);
	}
	
}
