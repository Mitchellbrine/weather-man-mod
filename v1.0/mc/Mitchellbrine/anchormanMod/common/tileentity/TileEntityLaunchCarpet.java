package mc.Mitchellbrine.anchormanMod.common.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityLaunchCarpet extends TileEntity {
	
	public double power = 1.0;
	
	
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setDouble("power", power);
	}
	
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("power"))
		{
			power = nbt.getDouble("power");
		}
		else
		{
			power = 1.0;
		}
	}

	
	public void invalidate()
	{
		
	}
	

}
