package mc.Mitchellbrine.anchormanMod.common.event;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.monster.EntitySkeleton;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.living.LivingDropsEvent;

public class LivingMagicalMurderEvent {
	
	
    public static double rand;

    @ForgeSubscribe
    public void onEntityDrop(LivingDropsEvent event) {
            if (event.source.getDamageType().equals("player")) {
                 rand = Math.random();
                 
                 Random random = new Random();
                 
                  if (event.entityLiving instanceof EntityEnderman) {
                       if (rand < 0.5d){
                    	  if (random.nextInt(1) == 0){
                          event.entityLiving.dropItem(MainMod.magicalPearl.itemID, random.nextInt(3));
                     }
                    	  else {
                           event.entityLiving.dropItem(MainMod.magicalWool.blockID, random.nextInt(3));                    		  
                    	  }
                     }
               }
                  else if (event.entityLiving instanceof EntityVillager) {
                	  if (rand < 0.5d) {
                		  if (((EntityVillager)event.entityLiving).getProfession() == 2) {
                			  event.entityLiving.dropItem(MainMod.magicalPearl2.itemID, random.nextInt(3));
                		  }
                		  else {
                			  event.entityLiving.dropItem(MainMod.magicalDayPearl.itemID, random.nextInt(3));    
                		  }
                		  
                		  }
                  }
                  else if (event.entityLiving instanceof EntitySkeleton) {
                	  if (((EntitySkeleton)event.entityLiving).getSkeletonType() == 1) {
                		  if (rand < 0.5d) {
                			  event.entityLiving.dropItem(MainMod.magicalNightPearl.itemID, random.nextInt(3));
                		  }
                	  }
                  }
                  }
            
    
    }
	

}
