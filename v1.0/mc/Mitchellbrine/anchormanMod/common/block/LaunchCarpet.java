package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityLaunchCarpet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;

public class LaunchCarpet extends BlockContainer {



	public LaunchCarpet(int par1) {
		super(par1, Material.cloth);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityLaunchCarpet();
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	
    	if (par1World.isRemote)
    	{
        	TileEntityLaunchCarpet te = (TileEntityLaunchCarpet) par1World.getBlockTileEntity(par2, par3, par4);

        if (!par5EntityPlayer.isSneaking())
        {
        par5EntityPlayer.motionY += te.power;	
        par5EntityPlayer.addChatMessage(EnumChatFormatting.GREEN + "Launch in progress!");
		return true;
        }
        else
        {
        	if (te.power == 1.5)
        	{
        		te.power = 2.0;
        		par5EntityPlayer.addChatMessage(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: HIGH");
        	}
        	else if (te.power == 1.0)
        	{
        		te.power = 1.5;
        		par5EntityPlayer.addChatMessage(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: MEDIUM");
        	}
        	else if (te.power == 2.0)
        	{
        		if (par5EntityPlayer.username.equalsIgnoreCase("direwolf20") || par5EntityPlayer.username.equalsIgnoreCase("soaryn") || par5EntityPlayer.username.equalsIgnoreCase("Dinnerbone") || par5EntityPlayer.username.equalsIgnoreCase("grumm") || par5EntityPlayer.username.equalsIgnoreCase("Mitchellbrine") || par5EntityPlayer.username.equalsIgnoreCase("sci4me"))
        		{
        			te.power = 9000000;
            		par5EntityPlayer.addChatMessage(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: " + EnumChatFormatting.DARK_RED + "" + EnumChatFormatting.ITALIC + "OVER 9000!");
        		}
        		else
        		{
        			te.power = 1.0;
            		par5EntityPlayer.addChatMessage(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: LOW");	
        		}
        	}
        	else if (te.power == 9000000)
        	{
    			te.power = 1.0;
        		par5EntityPlayer.addChatMessage(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: LOW");	        		
        	}
        }
    	}
    	
    	return false;
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
    public void registerIcons(IconRegister icon) {
            this.blockIcon = icon.registerIcon("anchorman:lIcon");
    }

	

}
