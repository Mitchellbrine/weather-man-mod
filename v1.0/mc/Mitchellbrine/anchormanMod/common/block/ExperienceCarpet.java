package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityExpCarpet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;

public class ExperienceCarpet extends BlockContainer {



	public ExperienceCarpet(int par1) {
		super(par1, Material.cloth);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityExpCarpet();
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	
    	if (!par1World.isRemote)
    	{
        if (par5EntityPlayer.isSneaking())
        {
        	if (par5EntityPlayer.inventory.hasItem(Item.expBottle.itemID) || par5EntityPlayer.capabilities.isCreativeMode)
        	{
        		par5EntityPlayer.inventory.consumeInventoryItem(Item.expBottle.itemID);
        		par5EntityPlayer.addExperienceLevel(1);
        		par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Experience granted!");
        	}
        	else
        	{
        		par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Bottles needed!");        		
        	}
		return true;
        }
        }
    	else if (par1World.isRemote)
    	{
        if (par5EntityPlayer.isSneaking())
        {
        	if (par5EntityPlayer.inventory.hasItem(Item.expBottle.itemID) || par5EntityPlayer.capabilities.isCreativeMode)
        	{
        		par5EntityPlayer.inventory.consumeInventoryItem(Item.expBottle.itemID);
        		par5EntityPlayer.addExperienceLevel(1);
        	}
        	else
        	{
        	}
		return true;
        }
        }
    	
    	return false;
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
    public void registerIcons(IconRegister icon) {
            this.blockIcon = icon.registerIcon("anchorman:expIcon");
    }

	

}
