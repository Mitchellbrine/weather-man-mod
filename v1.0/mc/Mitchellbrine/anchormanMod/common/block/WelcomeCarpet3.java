package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet3;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class WelcomeCarpet3 extends BlockContainer {



	public WelcomeCarpet3(int par1) {
		super(par1, Material.cloth);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
		this.setBlockBoundsForItemRender();
	}
	
	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityWelcomeCarpet3();
	}
	
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	if (!par1World.isRemote)
    	{
    		TileEntityWelcomeCarpet3 te = (TileEntityWelcomeCarpet3) par1World.getBlockTileEntity(par2, par3, par4);
    		
    		if (par5EntityPlayer.isSneaking())
    		{
    			par1World.setBlock(par2, par3, par4, MainMod.welcomeMat.blockID);
    			return true;
    		}
    		else
    		{
    			par5EntityPlayer.addChatMessage(EnumChatFormatting.GOLD + "Welcome " + par5EntityPlayer.username + "!");
    		}
    	}
    	return false;
    }
    
    
    public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity)
    {
    	if (!par1World.isRemote)
    	{
    		if (par5Entity instanceof EntityPlayer)
    		{
    				((EntityPlayer)par5Entity).addChatMessage(EnumChatFormatting.GOLD + "Welcome " + ((EntityPlayer)par5Entity).username + "!");
    		}
    	}
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
    public void registerIcons(IconRegister icon) {
            this.blockIcon = icon.registerIcon("anchorman:wIcon");
    }

    public int idDropped(int par1, Random par2Random, int par3)
    {
		return MainMod.welcomeMat.blockID;
    	
    }
    
    public int idDropped(World par1World, int par2, int par3, int par4)
    {
		return MainMod.welcomeMat.blockID;
    	
    }
	

}
