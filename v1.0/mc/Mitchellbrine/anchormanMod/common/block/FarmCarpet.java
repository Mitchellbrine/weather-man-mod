package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityFarmCarpet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class FarmCarpet extends BlockContainer {



	public FarmCarpet(int par1) {
		super(par1, Material.cloth);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileEntityFarmCarpet();
	}
	
	@Override
	public TileEntity createNewTileEntity(World world) {
		return new TileEntityFarmCarpet();
	}
	
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	if (!par1World.isRemote)
    	{
    		TileEntityFarmCarpet te = (TileEntityFarmCarpet) par1World.getBlockTileEntity(par2, par3, par4);
    		
    		if (par5EntityPlayer.isSneaking())
    		{
    			if (par5EntityPlayer.inventory.hasItem(Item.seeds.itemID) || par5EntityPlayer.inventory.hasItem(Item.pumpkinSeeds.itemID) || par5EntityPlayer.inventory.hasItem(Item.melonSeeds.itemID) || par5EntityPlayer.inventory.hasItem(Item.carrot.itemID) || par5EntityPlayer.inventory.hasItem(Item.potato.itemID))
    			{
    				if (par5EntityPlayer.inventory.hasItem(Item.seeds.itemID))
    				{
    				par5EntityPlayer.inventory.consumeInventoryItem(Item.seeds.itemID);
    				par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.wheat, 1));
    				par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Wheat grown!");
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Item.pumpkinSeeds.itemID))
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Item.pumpkinSeeds.itemID);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Block.pumpkin, 1));
        				par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Pumpkin grown!");
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Item.melonSeeds.itemID))
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Item.melonSeeds.itemID);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Block.melon, 1));
    					par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Melon grown!");
    				}
    				
    				if (par5EntityPlayer.inventory.hasItemStack(new ItemStack(Item.carrot)))
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.carrot, 1));
    					par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Carrot grown!");
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Item.potato.itemID))
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.potato, 1));
    					par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Potato grown!");
    				}
        			return true;
    			}
    			else
    			{
    				par5EntityPlayer.addChatMessage(EnumChatFormatting.YELLOW + "Seeds required for farming!");
    			}
    		}
    	}
    	else
    	{
    		TileEntityFarmCarpet te = (TileEntityFarmCarpet) par1World.getBlockTileEntity(par2, par3, par4);
    		
    		if (par5EntityPlayer.isSneaking())
    		{
    			if (par5EntityPlayer.inventory.hasItem(Item.seeds.itemID) || par5EntityPlayer.inventory.hasItem(Item.pumpkinSeeds.itemID) || par5EntityPlayer.inventory.hasItem(Item.melonSeeds.itemID) || par5EntityPlayer.inventory.hasItem(Item.carrot.itemID) || par5EntityPlayer.inventory.hasItem(Item.potato.itemID))
    			{
    				
    				if (par5EntityPlayer.inventory.hasItem(Item.seeds.itemID))
    				{
    				par5EntityPlayer.inventory.consumeInventoryItem(Item.seeds.itemID);
    				par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.wheat, 1));
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Item.pumpkinSeeds.itemID))
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Item.pumpkinSeeds.itemID);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Block.pumpkin, 1));
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Item.melonSeeds.itemID))
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Item.melonSeeds.itemID);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Block.melon, 1));
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Item.carrot.itemID))
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.carrot, 1));
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Item.potato.itemID))
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.potato, 1));
    				}
    				return true;
    			}
    		}    		
    	}
    	return false;
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
    public void registerIcons(IconRegister icon) {
            this.blockIcon = icon.registerIcon("anchorman:fIcon");
    }
    
    public int idDropped(int par1, Random par2Random, int par3)
    {
		return MainMod.farmCarpet.blockID;
    	
    }
	

}
