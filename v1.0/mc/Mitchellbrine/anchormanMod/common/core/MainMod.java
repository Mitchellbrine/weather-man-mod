package mc.Mitchellbrine.anchormanMod.common.core;

import mc.Mitchellbrine.anchormanMod.common.block.DawnSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.DuskSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.ExperienceCarpet;
import mc.Mitchellbrine.anchormanMod.common.block.FarmCarpet;
import mc.Mitchellbrine.anchormanMod.common.block.LaunchCarpet;
import mc.Mitchellbrine.anchormanMod.common.block.MidnightSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.NetherCarpet;
import mc.Mitchellbrine.anchormanMod.common.block.NoonSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.RainSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.SpawnSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.ThunderSummoner;
import mc.Mitchellbrine.anchormanMod.common.block.WelcomeCarpet;
import mc.Mitchellbrine.anchormanMod.common.block.WelcomeCarpet1;
import mc.Mitchellbrine.anchormanMod.common.block.WelcomeCarpet2;
import mc.Mitchellbrine.anchormanMod.common.block.WelcomeCarpet3;
import mc.Mitchellbrine.anchormanMod.common.event.LivingMagicalMurderEvent;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityDawnCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityDuskCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityExpCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityFarmCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityLaunchCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityMidnightCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityNetherCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityNoonCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityRainCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntitySpawnCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityThunderCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet1;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet2;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet3;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;


@NetworkMod(clientSideRequired = true, serverSideRequired = true)
@Mod(modid = "weatherManMod", name = "Weather Carpets Mod", version = "DEV BUILD", useMetadata = true)
public class MainMod {

	@SidedProxy(clientSide = "mc.Mitchellbrine.anchormanMod.client.core.ClientProxy", serverSide = "mc.Mitchellbrine.anchormanMod.common.core.CommonProxy")
	
	public static CommonProxy proxy;
	
	public static CreativeTabs ModTab = new ModTab(CreativeTabs.getNextID(), "weatherCarpet");
	
	// Config Ids
	
	public static int rSID;
	public static int tSID;
	public static int nSID;
	public static int mSID;
	public static int daSID;
	public static int duSID;
	public static int sSID;
	public static int lCID;
	public static int welcomeMatNorthID;
	public static int welcomeMatEastID;
	public static int welcomeMatSouthID;
	public static int welcomeMatWestID;
	public static int expCID;
	public static int mW;
	public static int mW2;
	public static int mDW;
	public static int mNW;
	public static int bMID;
	public static int fCID;
	public static int mP;
	public static int mP2;
	public static int mDP;
	public static int mNP;
	public static int hSID;
	
	
	
	// Carpets
	
	public static Block rainSummoner;
	public static Block thunderSummoner;
	public static Block noonSummoner;
	public static Block midnightSummoner;
	public static Block dawnSummoner;
	public static Block duskSummoner;
	public static Block spawnSummoner;
	public static Block launcherCarpet;
	public static Block welcomeMat;
	public static Block welcomeMatEast;
	public static Block welcomeMatSouth;
	public static Block welcomeMatWest;
	public static Block expCarpet;
	public static Block bonemealBlock;
	public static Block farmCarpet;
	public static Block netherCarpet;

	
	// Wool
	
	public static Block magicalWool;
	public static Block magicalWool2;

	public static Block magicalDayWool;
	public static Block magicalNightWool;
	
	// Pearls
	
	public static Item magicalPearl;
	public static Item magicalPearl2;
	
	public static Item magicalDayPearl;
	public static Item magicalNightPearl;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
	Configuration config = new Configuration(event.getSuggestedConfigurationFile());

	config.load();

	rSID = config.get(Configuration.CATEGORY_BLOCK, "rS", 230).getInt();
	tSID = config.get(Configuration.CATEGORY_BLOCK, "tS", 231).getInt();
	nSID = config.get(Configuration.CATEGORY_BLOCK, "nS", 232).getInt();
	mSID = config.get(Configuration.CATEGORY_BLOCK, "mS", 234).getInt();
	daSID = config.get(Configuration.CATEGORY_BLOCK, "daS", 235).getInt();
	duSID = config.get(Configuration.CATEGORY_BLOCK, "duS", 236).getInt();
	sSID = config.get(Configuration.CATEGORY_BLOCK, "sS", 237).getInt();
	lCID = config.get(Configuration.CATEGORY_BLOCK, "lC", 238).getInt();
	welcomeMatNorthID = config.get(Configuration.CATEGORY_BLOCK, "wM", 239).getInt();
	welcomeMatEastID = config.get(Configuration.CATEGORY_BLOCK, "wM1", 240).getInt();
	welcomeMatSouthID = config.get(Configuration.CATEGORY_BLOCK, "wM2", 241).getInt();
	welcomeMatWestID = config.get(Configuration.CATEGORY_BLOCK, "wM3", 242).getInt();
	expCID = config.get(Configuration.CATEGORY_BLOCK, "expC", 243).getInt();
	fCID = config.get(Configuration.CATEGORY_BLOCK, "fC", 244).getInt();
	hSID = config.get(Configuration.CATEGORY_BLOCK, "hSID", 250).getInt();
	
	mW = config.get(Configuration.CATEGORY_BLOCK, "mW", 245).getInt();
	mW2 = config.get(Configuration.CATEGORY_BLOCK, "mW2", 246).getInt();
	mDW = config.get(Configuration.CATEGORY_BLOCK, "mdW", 247).getInt();
	mNW = config.get(Configuration.CATEGORY_BLOCK, "mNW", 248).getInt();
	bMID = config.get(Configuration.CATEGORY_BLOCK, "bM", 249).getInt();
	
	mP = config.get(Configuration.CATEGORY_ITEM, "mP", 4000).getInt();
	mP2 = config.get(Configuration.CATEGORY_ITEM, "mP2", 4001).getInt();
	mDP = config.get(Configuration.CATEGORY_ITEM, "mDP", 4002).getInt();
	mNP = config.get(Configuration.CATEGORY_ITEM, "mNP", 4003).getInt();
	
	
	config.save();
	
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		proxy.registerTileEntities();
		
		rainSummoner = new RainSummoner(rSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:rainSummoner");
		thunderSummoner = new ThunderSummoner(tSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:thunderSummoner");
		noonSummoner = new NoonSummoner(nSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:noonSummoner");
		midnightSummoner = new MidnightSummoner(mSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:midnightSummoner");
		dawnSummoner = new DawnSummoner(daSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:dawnSummoner");
		duskSummoner = new DuskSummoner(duSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:duskSummoner");
		spawnSummoner = new SpawnSummoner(sSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:spawnSummoner");
		launcherCarpet = new LaunchCarpet(lCID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:launchCarpet");
		welcomeMat = new WelcomeCarpet(welcomeMatNorthID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:welcomeMat");
		welcomeMatEast = new WelcomeCarpet1(welcomeMatEastID).setUnlocalizedName("anchorman:welcomeMatEast");
		welcomeMatSouth = new WelcomeCarpet2(welcomeMatSouthID).setUnlocalizedName("anchorman:welcomeMatSouth");
		welcomeMatWest = new WelcomeCarpet3(welcomeMatWestID).setUnlocalizedName("anchorman:welcomeMatWest");
		expCarpet = new ExperienceCarpet(expCID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:expSummoner");
		farmCarpet = new FarmCarpet(fCID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:farmSummoner");
		netherCarpet = new NetherCarpet(hSID).setCreativeTab(ModTab).setUnlocalizedName("anchorman:netherSummoner");

		magicalWool = new Block(mW, Material.cloth).setHardness(0.8F).setStepSound(Block.soundClothFootstep).setCreativeTab(ModTab).setUnlocalizedName("anchorman:magicalWool").setTextureName("anchorman:magicalWool");
		magicalWool2 = new Block(mW2, Material.cloth).setHardness(0.8F).setStepSound(Block.soundClothFootstep).setCreativeTab(ModTab).setUnlocalizedName("anchorman:boundMagicalWool").setTextureName("anchorman:boundMagicalWool");
		magicalDayWool = new Block(mDW, Material.cloth).setHardness(0.8F).setStepSound(Block.soundClothFootstep).setCreativeTab(ModTab).setUnlocalizedName("anchorman:dayWool").setTextureName("anchorman:dayWool");
		magicalNightWool = new Block(mNW, Material.cloth).setHardness(0.8F).setStepSound(Block.soundClothFootstep).setCreativeTab(ModTab).setUnlocalizedName("anchorman:nightWool").setTextureName("anchorman:nightWool");
		bonemealBlock = new Block(bMID, Material.plants).setHardness(4.0F).setCreativeTab(ModTab).setUnlocalizedName("anchorman:boneMeal").setTextureName("anchorman:boneMealBlock");
		
		magicalPearl = new Item(mP).setCreativeTab(ModTab).setUnlocalizedName("anchorman:magicalPearl").setTextureName("anchorman:magicalPearl");
		magicalPearl2 = new Item(mP2).setCreativeTab(ModTab).setUnlocalizedName("anchorman:boundMagicalPearl").setTextureName("anchorman:magicalPearl2");
		magicalDayPearl = new Item(mDP).setCreativeTab(ModTab).setUnlocalizedName("anchorman:magicalDayPearl").setTextureName("anchorman:magicalDayPearl");
		magicalNightPearl = new Item(mNP).setCreativeTab(ModTab).setUnlocalizedName("anchorman:magicalNightPearl").setTextureName("anchorman:magicalNightPearl");
		
		
		
		GameRegistry.registerBlock(rainSummoner, "anchorman:rainSummoner");
		GameRegistry.registerBlock(thunderSummoner, "anchorman:thunderSummoner");
		GameRegistry.registerBlock(noonSummoner, "anchorman:noonSummoner");
		GameRegistry.registerBlock(midnightSummoner, "anchorman:midnightSummoner");
		GameRegistry.registerBlock(dawnSummoner, "anchorman:dawnSummoner");
		GameRegistry.registerBlock(duskSummoner, "anchorman:duskSummoner");
		GameRegistry.registerBlock(spawnSummoner, "anchorman:spawnSummoner");
		GameRegistry.registerBlock(magicalWool, "anchorman:magicalWool");
		GameRegistry.registerBlock(magicalWool2, "anchorman:boundMagicalWool");
		GameRegistry.registerBlock(magicalDayWool, "anchorman:magicalDayWool");
		GameRegistry.registerBlock(magicalNightWool, "anchorman:magicalNightWool");
		GameRegistry.registerBlock(launcherCarpet, "anchorman:launchCarpet");
		GameRegistry.registerBlock(welcomeMat, "anchorman:welcomeMat");
		GameRegistry.registerBlock(welcomeMatEast, "anchorman:welcomeMatEast");
		GameRegistry.registerBlock(welcomeMatSouth, "anchorman:welcomeMatSouth");
		GameRegistry.registerBlock(welcomeMatWest, "anchorman:welcomeMatWest");
		GameRegistry.registerBlock(expCarpet, "anchorman:expSummoner");
		GameRegistry.registerBlock(bonemealBlock, "anchorman:boneMeal");
		GameRegistry.registerBlock(farmCarpet, "anchorman:farmSummoner");
		GameRegistry.registerBlock(netherCarpet, "anchorman:netherCarpet");
		
		GameRegistry.registerItem(magicalPearl, "anchorman:magicalPearl");
		GameRegistry.registerItem(magicalPearl2, "anchorman:boundMagicalPearl");
		GameRegistry.registerItem(magicalDayPearl, "anchorman:magicalDayPearl");
		GameRegistry.registerItem(magicalNightPearl, "anchorman:magicalNightPearl");

		LanguageRegistry.addName(rainSummoner, "Rain Carpet");
		LanguageRegistry.addName(thunderSummoner, "Thunder Carpet");
		LanguageRegistry.addName(noonSummoner, "Noon Carpet");
		LanguageRegistry.addName(midnightSummoner, "Midnight Carpet");
		LanguageRegistry.addName(dawnSummoner, "Dawn Carpet");
		LanguageRegistry.addName(duskSummoner, "Dusk Carpet");
		LanguageRegistry.addName(spawnSummoner, "Spawn Carpet");
		LanguageRegistry.addName(netherCarpet, "Nether Carpet");
		LanguageRegistry.addName(magicalWool, "Magical Wool");
		LanguageRegistry.addName(magicalWool2, "Thick Magical Wool");
		LanguageRegistry.addName(magicalDayWool, "Magical Day Wool");
		LanguageRegistry.addName(magicalNightWool, "Magical Night Wool");
		LanguageRegistry.addName(launcherCarpet, "Launch Carpet");
		LanguageRegistry.addName(welcomeMat, "Welcome Mat");
		LanguageRegistry.addName(welcomeMatEast, "Welcome Mat");
		LanguageRegistry.addName(welcomeMatSouth, "Welcome Mat");
		LanguageRegistry.addName(welcomeMatWest, "Welcome Mat");
		LanguageRegistry.addName(expCarpet, "Carpet O' Experience");
		LanguageRegistry.addName(bonemealBlock, "Block of Bonemeal");
		LanguageRegistry.addName(farmCarpet, "Farm Carpet");
		LanguageRegistry.addName(magicalPearl, "Magical Pearl");
		LanguageRegistry.addName(magicalPearl2, "Thick Magical Pearl");
		LanguageRegistry.addName(magicalDayPearl, "Magical Day Pearl");
		LanguageRegistry.addName(magicalNightPearl, "Magical Night Pearl");
		
		OreDictionary.registerOre(bMID, bonemealBlock);
		
		LanguageRegistry.instance().addStringLocalization("itemGroup.weatherCarpet", "Weather Carpets Mod");
		
		MinecraftForge.EVENT_BUS.register(new LivingMagicalMurderEvent());
		
		GameRegistry.registerTileEntity(TileEntityRainCarpet.class, "anchorman:rainCarpet");
		GameRegistry.registerTileEntity(TileEntityThunderCarpet.class, "anchorman:thunderCarpet");
		GameRegistry.registerTileEntity(TileEntityNoonCarpet.class, "anchorman:noonCarpet");
		GameRegistry.registerTileEntity(TileEntityMidnightCarpet.class, "anchorman:midnightCarpet");
		GameRegistry.registerTileEntity(TileEntityDawnCarpet.class, "anchorman:dawnCarpet");
		GameRegistry.registerTileEntity(TileEntityDuskCarpet.class, "anchorman:duskCarpet");
		GameRegistry.registerTileEntity(TileEntitySpawnCarpet.class, "anchorman:spawnCarpet");
		GameRegistry.registerTileEntity(TileEntityLaunchCarpet.class, "anchorman:launchCarpet");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet.class, "anchorman:welcomeCarpet");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet1.class, "anchorman:welcomeCarpet1");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet2.class, "anchorman:welcomeCarpet2");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet3.class, "anchorman:welcomeCarpet3");
		GameRegistry.registerTileEntity(TileEntityExpCarpet.class, "anchorman:expCarpet");
		GameRegistry.registerTileEntity(TileEntityFarmCarpet.class, "anchorman:farmCarpet");
		GameRegistry.registerTileEntity(TileEntityNetherCarpet.class, "anchorman:netherCarpet");

		
		GameRegistry.addSmelting(magicalPearl.itemID, new ItemStack(magicalPearl2, 1), 0.25F);
		GameRegistry.addRecipe(new ItemStack(magicalWool2, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalPearl2,Character.valueOf('Y'), magicalWool);
		GameRegistry.addRecipe(new ItemStack(rainSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Item.bucketWater);
		GameRegistry.addRecipe(new ItemStack(thunderSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Block.blockIron);
		GameRegistry.addRecipe(new ItemStack(magicalDayWool , 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayPearl);
		GameRegistry.addRecipe(new ItemStack(magicalNightWool , 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalNightPearl);
		GameRegistry.addRecipe(new ItemStack(dawnSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool);
		GameRegistry.addRecipe(new ItemStack(duskSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalNightWool);
		GameRegistry.addRecipe(new ItemStack(noonSummoner, 1), "XYX", "XXX", "XZX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool, Character.valueOf('Z'), magicalNightWool);
		GameRegistry.addRecipe(new ItemStack(midnightSummoner, 1), "XZX", "XXX", "XYX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool, Character.valueOf('Z'), magicalNightWool);
		GameRegistry.addRecipe(new ItemStack(spawnSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Item.bed);
		GameRegistry.addRecipe(new ItemStack(launcherCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Block.tnt);
		GameRegistry.addRecipe(new ItemStack(welcomeMat, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), new ItemStack(Block.cloth, 1, 12), Character.valueOf('Y'), Item.sign);
		GameRegistry.addRecipe(new ItemStack(expCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Block.bookShelf);
		GameRegistry.addRecipe(new ItemStack(bonemealBlock, 1), "XXX", "XXX", "XXX", Character.valueOf('X'), new ItemStack(Item.dyePowder, 1, 15));
		GameRegistry.addRecipe(new ItemStack(farmCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), bonemealBlock);
		GameRegistry.addRecipe(new ItemStack(netherCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Item.blazePowder);
		
		GameRegistry.addShapelessRecipe(new ItemStack(Item.dyePowder, 9, 15), bonemealBlock);
		GameRegistry.addShapelessRecipe(new ItemStack(Block.dragonEgg, 2), Block.dragonEgg);
		
	}
	

}
