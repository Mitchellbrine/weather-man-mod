package mc.Mitchellbrine.anchormanMod.client.render;

import mc.Mitchellbrine.anchormanMod.client.model.ModelRainSummoner;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityFarmCarpet;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class FCarpetHandler implements IItemRenderer{

	private ModelRainSummoner model;
	
	public FCarpetHandler() {
		model = new ModelRainSummoner();
	}
	
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		TileEntityRenderer.instance.renderTileEntityAt(new TileEntityFarmCarpet(), 0.0, 0.0, 0.0, 0.0f);
	}

}
