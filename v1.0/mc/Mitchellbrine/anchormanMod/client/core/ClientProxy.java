package mc.Mitchellbrine.anchormanMod.client.core;

import mc.Mitchellbrine.anchormanMod.client.render.DaCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.DuCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.ExpCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.FCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.HCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.LCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.MCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.NCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.RSHandler;
import mc.Mitchellbrine.anchormanMod.client.render.SCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.TCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.render.WCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityDaCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityDuCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityExpCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityFCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityHCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityLCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityMCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityNCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntitySCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityTCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityWCarpetHandler;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityWCarpetHandler1;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityWCarpetHandler2;
import mc.Mitchellbrine.anchormanMod.client.tileentity.TileEntityWCarpetHandler3;
import mc.Mitchellbrine.anchormanMod.common.core.CommonProxy;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityDawnCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityDuskCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityExpCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityFarmCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityLaunchCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityMidnightCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityNetherCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityNoonCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityRainCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntitySpawnCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityThunderCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet1;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet2;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet3;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.registry.ClientRegistry;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerTileEntities() {
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRainCarpet.class, new TileEntityCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityThunderCarpet.class, new TileEntityTCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMidnightCarpet.class, new TileEntityMCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityNoonCarpet.class, new TileEntityNCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDawnCarpet.class, new TileEntityDaCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDuskCarpet.class, new TileEntityDuCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySpawnCarpet.class, new TileEntitySCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityLaunchCarpet.class, new TileEntityLCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet.class, new TileEntityWCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet1.class, new TileEntityWCarpetHandler1());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet2.class, new TileEntityWCarpetHandler2());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet3.class, new TileEntityWCarpetHandler3());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityExpCarpet.class, new TileEntityExpCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFarmCarpet.class, new TileEntityFCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityNetherCarpet.class, new TileEntityHCarpetHandler());
		
		MinecraftForgeClient.registerItemRenderer(MainMod.rSID, new RSHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.tSID, new TCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.daSID, new DaCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.nSID, new NCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.mSID, new MCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.duSID, new DuCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.sSID, new SCarpetHandler()); 
		MinecraftForgeClient.registerItemRenderer(MainMod.welcomeMatNorthID, new WCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.fCID, new FCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.lCID, new LCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.expCID, new ExpCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(MainMod.hSID, new HCarpetHandler());
	}
	
}
