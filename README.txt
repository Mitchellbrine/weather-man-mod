Weather Carpets Mod (An Official JamCraft Mod)
-------------------------------
My mod for Modjam 3.

Enjoy controlling extreme weather at your fingertips... or your feet.

MOD USE
---
To start, you need Magical Wool. It drops from Endermen. 9 Magical Wool make a Thick Magical Wool, which can be used to create all but one carpet.

There are too many crafting recipes to list... 13 to the exact.

So, check the wiki at https://bitbucket.org/Mitchellbrine/weather-man-mod/wiki/Home for more info!
---