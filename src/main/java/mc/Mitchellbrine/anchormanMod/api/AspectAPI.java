package mc.Mitchellbrine.anchormanMod.api;

import java.util.List;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class AspectAPI extends Item{
	
	private String aspectName;
	
	public AspectAPI(String name, String modid, CreativeTabs aspectTab) {
		setTextureName(modid + ":aspects/aspect"+name);
		setUnlocalizedName("aspect"+name);
		setCreativeTab(aspectTab);
		aspectName = name;
	}
	
	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add("Type: " + aspectName);
	}
	
}
