package mc.Mitchellbrine.anchormanMod.api;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.IItemRenderer;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;


@SideOnly(Side.CLIENT)
/* Note all instances of this class should be uses in the proxy! Anywhere else would be an SMP issue!*/
public class ClientRegistering {

	
/* Registers a carpet more efficiently and in one line of code */	
public void registerCarpet(Class <? extends TileEntity> tileEntityClass, TileEntitySpecialRenderer renderManager, ItemBlock carpet, IItemRenderer carpet3DRenderer) {
	ClientRegistry.bindTileEntitySpecialRenderer(tileEntityClass, renderManager);
	MinecraftForgeClient.registerItemRenderer(carpet, carpet3DRenderer);
}
	
	
}
