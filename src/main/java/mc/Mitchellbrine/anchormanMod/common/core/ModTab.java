package mc.Mitchellbrine.anchormanMod.common.core;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.util.StringTranslate;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

final class ModTab extends CreativeTabs
{


    ModTab(int par1)
    {
        super(par1, MainMod.getVersionString());
    }

    @SideOnly(Side.CLIENT)

	@Override
	public Item getTabIconItem() {
    	if (MainMod.versionType == 1) {
		return MainMod.magicalNightPearl;
    	}
    	else if (MainMod.versionType == 2) {
    		return MainMod.magicalPearl2;
    	}
    	return MainMod.magicalDayPearl;
    	
    }

}