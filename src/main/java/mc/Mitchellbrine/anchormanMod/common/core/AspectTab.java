package mc.Mitchellbrine.anchormanMod.common.core;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

final class AspectTab extends CreativeTabs
{
    AspectTab(int par1, String par2Str)
    {
        super(par1, par2Str);
    }

    @SideOnly(Side.CLIENT)

	@Override
	public Item getTabIconItem() {
    	return MainMod.aspectForma;
    	
    }
}