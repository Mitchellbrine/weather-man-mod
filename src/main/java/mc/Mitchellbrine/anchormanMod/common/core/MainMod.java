package mc.Mitchellbrine.anchormanMod.common.core;

import cpw.mods.fml.common.Loader;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import mc.Mitchellbrine.anchormanMod.aspects.Aspect;
import mc.Mitchellbrine.anchormanMod.aspects.AspectEvents;
import mc.Mitchellbrine.anchormanMod.client.render.RSHandler;
import mc.Mitchellbrine.anchormanMod.common.block.*;
import mc.Mitchellbrine.anchormanMod.common.event.CraftingHandler;
import mc.Mitchellbrine.anchormanMod.common.event.LivingMagicalMurderEvent;
import mc.Mitchellbrine.anchormanMod.common.item.*;
import mc.Mitchellbrine.anchormanMod.common.tileentity.*;
import mc.Mitchellbrine.anchormanMod.nei.FormerRecipeHandler;
import mc.Mitchellbrine.anchormanMod.nei.FormerRecipeRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Achievement;
import net.minecraft.world.biome.BiomeGenTaiga;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

import java.util.ArrayList;
import java.util.HashMap;

@Mod(modid = "weatherManMod", name = "Weather Carpets Mod", version = "RELEASE", useMetadata = true, guiFactory = "mc.Mitchellbrine.anchormanMod.util.config.WCModGui")
public class MainMod {

	@SidedProxy(clientSide = "mc.Mitchellbrine.anchormanMod.client.core.ClientProxy", serverSide = "mc.Mitchellbrine.anchormanMod.common.core.CommonProxy")
	public static CommonProxy proxy;

	public static String MOD_ID = "anchorman";

    public static int versionType = 0;

    public static CreativeTabs ModTab = new ModTab(CreativeTabs.getNextID());

	public static CreativeTabs AspectTab = new AspectTab(CreativeTabs.getNextID(), "weatherCarpetAspect");

    public static HashMap<WeatherCarpet,Aspect> aspects1 = new HashMap<WeatherCarpet, Aspect>();
    public static HashMap<WeatherCarpet,Aspect> aspects2 = new HashMap<WeatherCarpet, Aspect>();
    public static HashMap<WeatherCarpet,Aspect> aspects3 = new HashMap<WeatherCarpet, Aspect>();

	public static Configuration config;

	// Carpet Former
	
	public static Block carpetFormer;
	public static Block carpetFormerFilled;
	public static Block carpetFormerFilledWool;
	
	// Carpets
	
	public static Block rainSummoner;
	public static Block thunderSummoner;
	public static Block noonSummoner;
	public static Block midnightSummoner;
	public static Block dawnSummoner;
	public static Block duskSummoner;
	public static Block spawnSummoner;
	public static Block launcherCarpet;
	public static Block welcomeMat;
	public static Block welcomeMatEast;
	public static Block welcomeMatSouth;
	public static Block welcomeMatWest;
	public static Block expCarpet;
	public static Block bonemealBlock;
	public static Block farmCarpet;
	public static Block netherCarpet;
    public static Block miningCarpet;
    public static Block detectionCarpet;
    public static Block detectionCarpet1;
    public static Block detectionCarpet2;
    public static Block trappedCarpet;
	
	// Wool
	
	public static Block magicalWool;
	public static Block magicalWool2;

	public static Block magicalDayWool;
	public static Block magicalNightWool;
	
	public static Block noonWool;
	public static Block midnightWool;
	
	public static Block dawnWool;
	public static Block duskWool;
	
	// Pearls
	
	public static Item magicalPearl;
	public static Item magicalPearl2;
	
	public static Item magicalDayPearl;
	public static Item magicalNightPearl;
	
	public static Item noonPearl;
	public static Item midnightPearl;
	
	public static Item dawnPearl;
	public static Item duskPearl;
	
	// Needle Stuff
	
	public static Item needle;
	public static Item needleWithThread;
	public static Item needleWithLinearThread;
	
	public static Item linearThread;
	
	public static Item magicalCarpetTemplate;
	public static Item woolCarpetTemplate;
	
	public static Item researchBook;

    public static Item spectacles;
	
	// Aspects
	
	public static Aspect aspectForma;
	public static Aspect aspectTime;
    public static Aspect aspectCharge;
    public static Aspect aspectRain;
    public static Aspect aspectNight;
    public static Aspect aspectDay;
    public static Aspect aspectCharm;
    public static Aspect aspectSky;
    public static Aspect aspectHome;
    public static Aspect aspectHell;
    public static Aspect aspectEarth;
    public static Aspect aspectDeath;
    public static Aspect aspectVoice;
	
	// Version Info
	
	public static String lastStableVersion = "1.1";
	public static String lastBetaVersion = "1.1.0";
	public static String lastDevVersion = "14w09a";
	
	// Skill System
	
	public static Achievement weaving;
	public static Achievement carpetCreation;
	public static Achievement linearMastery;
	public static Achievement woolenMastery;
	public static Achievement noLongerOfftopic;
	
	public static AchievementPage weatherCarpetsPage;
	
	// Config Objects

	public static int versionChannel;
	public static int over9000Enabled;
	public static int isUserValidated;
	public static boolean stopBuggingMe;
	public static boolean legacyRecipes;
	public static int notified = 0;
	public static boolean notifyAll;

	
	// Skill Integers
	
//	public static int weaveAchieve;
//	public static int carpetAchieve;
//	public static int linearAchieve;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
	config = new Configuration(event.getSuggestedConfigurationFile());

    syncConfig();

	carpetFormer = new CarpetFormer().setStepSound(Block.soundTypeWood).setCreativeTab(ModTab).setBlockName("carpetFormer").setBlockTextureName("anchorman:cFIcon");
	carpetFormerFilled = new CarpetFormerFilled().setStepSound(Block.soundTypeCloth).setLightLevel(0.4F).setBlockName("carpetFormerFilled").setBlockTextureName("anchorman:cFIcon");
	carpetFormerFilledWool = new CarpetFormerFilledWool().setStepSound(Block.soundTypeCloth).setBlockName("carpetFormerWool").setBlockTextureName("anchorman:cFIcon");
	
	rainSummoner = new RainSummoner().setCreativeTab(ModTab).setBlockName("rainSummoner").setBlockTextureName("anchorman:rSIcon");
	thunderSummoner = new ThunderSummoner().setCreativeTab(ModTab).setBlockName("thunderSummoner").setBlockTextureName("anchorman:tSIcon");
	noonSummoner = new NoonSummoner().setCreativeTab(ModTab).setBlockName("noonSummoner").setBlockTextureName("anchorman:nSIcon");
	midnightSummoner = new MidnightSummoner().setCreativeTab(ModTab).setBlockName("midnightSummoner").setBlockTextureName("anchorman:mSIcon");
	dawnSummoner = new DawnSummoner().setCreativeTab(ModTab).setBlockName("dawnSummoner").setBlockTextureName("anchorman:daSIcon");
	duskSummoner = new DuskSummoner().setCreativeTab(ModTab).setBlockName("duskSummoner").setBlockTextureName("anchorman:duSIcon");
	spawnSummoner = new SpawnSummoner().setCreativeTab(ModTab).setBlockName("spawnSummoner").setBlockTextureName("anchorman:sSIcon");
	launcherCarpet = new LaunchCarpet().setCreativeTab(ModTab).setBlockName("launchCarpet").setBlockTextureName("anchorman:lIcon");
	welcomeMat = new WelcomeCarpet().setCreativeTab(ModTab).setBlockName("welcomeMat").setBlockTextureName("anchorman:wIcon");
	welcomeMatEast = new WelcomeCarpet1().setBlockName("welcomeMatEast").setBlockTextureName("anchorman:wIcon");
	welcomeMatSouth = new WelcomeCarpet2().setBlockName("welcomeMatSouth").setBlockTextureName("anchorman:wIcon");
	welcomeMatWest =  new WelcomeCarpet3().setBlockName("welcomeMatWest").setBlockTextureName("anchorman:wIcon");
	expCarpet = new ExperienceCarpet().setCreativeTab(ModTab).setBlockName("expSummoner").setBlockTextureName("anchorman:expIcon");
	farmCarpet = new FarmCarpet().setCreativeTab(ModTab).setBlockName("farmSummoner").setBlockTextureName("anchorman:fIcon");
	netherCarpet = new NetherCarpet().setCreativeTab(ModTab).setBlockName("netherSummoner").setBlockTextureName("anchorman:hIcon");
    miningCarpet = new MiningCarpet().setCreativeTab(ModTab).setBlockName("miningCarpet").setBlockTextureName("anchorman:miIcon");
    trappedCarpet = new TrappedCarpet().setCreativeTab(ModTab).setBlockName("trappedCarpet").setBlockTextureName("anchorman:trIcon");
    detectionCarpet = new DetectionCarpet(0).setCreativeTab(ModTab).setBlockName("detectionCarpet").setBlockTextureName("anchorman:deIcon");
    detectionCarpet1 = new DetectionCarpet(1).setBlockName("detectionCarpet").setBlockTextureName("anchorman:deIcon");
    detectionCarpet2 = new DetectionCarpet(2).setBlockName("detectionCarpet").setBlockTextureName("anchorman:deIcon");


    magicalWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("magicalWool").setBlockTextureName("anchorman:magicalWool");
	magicalWool2 = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("boundMagicalWool").setBlockTextureName("anchorman:boundMagicalWool");
	magicalDayWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("dayWool").setBlockTextureName("anchorman:dayWool");
	magicalNightWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("nightWool").setBlockTextureName("anchorman:nightWool");
	noonWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("noonWool").setBlockTextureName("anchorman:noonWool");
	midnightWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("midnightWool").setBlockTextureName("anchorman:midnightWool");
	dawnWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("dawnWool").setBlockTextureName("anchorman:dawnWool");
	duskWool = new EmptyBlock(Material.cloth).setHardness(0.8F).setStepSound(Block.soundTypeCloth).setCreativeTab(ModTab).setBlockName("duskWool").setBlockTextureName("anchorman:duskWool");
	bonemealBlock = new EmptyBlock(Material.plants).setHardness(4.0F).setCreativeTab(ModTab).setBlockName("boneMeal").setBlockTextureName("anchorman:boneMealBlock");
	
	magicalPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("magicalPearl").setTextureName("anchorman:" + "magicalPearl");
	magicalPearl2 = new Item().setCreativeTab(ModTab).setUnlocalizedName("boundMagicalPearl").setTextureName("anchorman:" + "magicalPearl2");
	magicalDayPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("magicalDayPearl").setTextureName("anchorman:" + "magicalDayPearl");
	magicalNightPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("magicalNightPearl").setTextureName("anchorman:"+ "magicalNightPearl");
	noonPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("noonPearl").setTextureName("anchorman:noonPearl");
	midnightPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("midnightPearl").setTextureName("anchorman:midnightPearl");
	dawnPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("dawnPearl").setTextureName("anchorman:dawnPearl");
	duskPearl = new Item().setCreativeTab(ModTab).setUnlocalizedName("duskPearl").setTextureName("anchorman:duskPearl");
	
	needle = new Needle().setCreativeTab(ModTab).setUnlocalizedName("needle").setTextureName("anchorman:needle");
	needleWithThread = new NeedleWithThread().setCreativeTab(ModTab).setUnlocalizedName("needleWithThread").setTextureName("anchorman:needleThread");
	needleWithLinearThread = new NeedleWithLThread().setCreativeTab(ModTab).setUnlocalizedName("needleWithLinearThread").setTextureName("anchorman:needleLThread");
	
	linearThread = new Item().setCreativeTab(ModTab).setUnlocalizedName("linearThread").setTextureName("anchorman:linearThread");
	
	magicalCarpetTemplate = new Item().setCreativeTab(ModTab).setUnlocalizedName("carpetTemplate").setTextureName("anchorman:carpetTemplate");
	woolCarpetTemplate = new Item().setCreativeTab(ModTab).setUnlocalizedName("matTemplate").setTextureName("anchorman:matTemplate");

    //spectacles = new WCArmor(ItemArmor.ArmorMaterial.CLOTH,0,0).setCreativeTab(ModTab).setUnlocalizedName("aspectualSpectacles");

	//researchBook = new ResearchBook().setCreativeTab(ModTab).setUnlocalizedName("researchBookBlank").setTextureName("anchorman:researchBook");
	
	aspectForma = new Aspect("Forma");
	aspectTime = new Aspect("Aetas");
    aspectCharge = new Aspect("Inpero");
    aspectRain = new Aspect("Pluviae");
    aspectNight = new Aspect("Nox");
    aspectDay = new Aspect("Lux");
    aspectCharm = new Aspect("Amuletum");
    aspectSky = new Aspect("Caelum");
    aspectHome = new Aspect("Domum");
    aspectHell = new Aspect("Infernus");
    aspectEarth = new Aspect("Terra");
    aspectDeath = new Aspect("Mortem");
    aspectVoice = new Aspect("Vox");
	
	GameRegistry.registerBlock(rainSummoner, "rainSummoner");
	GameRegistry.registerBlock(thunderSummoner, "thunderSummoner");
	GameRegistry.registerBlock(noonSummoner, "noonSummoner");
	GameRegistry.registerBlock(midnightSummoner, "midnightSummoner");
	GameRegistry.registerBlock(dawnSummoner, "dawnSummoner");
	GameRegistry.registerBlock(duskSummoner, "duskSummoner");
	GameRegistry.registerBlock(spawnSummoner, "spawnSummoner");
	GameRegistry.registerBlock(launcherCarpet, "launchCarpet");
	GameRegistry.registerBlock(welcomeMat, "welcomeMat");
	GameRegistry.registerBlock(welcomeMatEast, "welcomeMatEast");
	GameRegistry.registerBlock(welcomeMatSouth, "welcomeMatSouth");
	GameRegistry.registerBlock(welcomeMatWest, "welcomeMatWest");
	GameRegistry.registerBlock(farmCarpet, "farmSummoner");
	GameRegistry.registerBlock(netherCarpet, "netherCarpet");
	GameRegistry.registerBlock(expCarpet, "expSummoner");
    GameRegistry.registerBlock(miningCarpet, "miningCarpet");
    GameRegistry.registerBlock(trappedCarpet,"trappedCarpet");
    GameRegistry.registerBlock(detectionCarpet,"detectionCarpet");
    GameRegistry.registerBlock(detectionCarpet1,"detectionCarpet1");
    GameRegistry.registerBlock(detectionCarpet2,"detectionCarpet2");
	GameRegistry.registerBlock(carpetFormer, "carpetFormer");
	GameRegistry.registerBlock(carpetFormerFilled, "carpetFormerFilled");
	GameRegistry.registerBlock(carpetFormerFilledWool, "carpetFormerFilledWool");
	GameRegistry.registerBlock(magicalWool, "magicalWool");
	GameRegistry.registerBlock(magicalWool2, "boundMagicalWool");
	GameRegistry.registerBlock(magicalDayWool, "magicalDayWool");
	GameRegistry.registerBlock(magicalNightWool, "magicalNightWool");
	GameRegistry.registerBlock(noonWool, "noonWool");
	GameRegistry.registerBlock(midnightWool, "midnightWool");
	GameRegistry.registerBlock(dawnWool, "dawnWool");
	GameRegistry.registerBlock(duskWool, "duskWool");
    GameRegistry.registerBlock(bonemealBlock, "boneMeal");

        //RegisterFluids.registerFluids();
	
	GameRegistry.registerItem(magicalPearl, "magicalPearl");
	GameRegistry.registerItem(magicalPearl2, "boundMagicalPearl");
	GameRegistry.registerItem(magicalDayPearl, "magicalDayPearl");
	GameRegistry.registerItem(magicalNightPearl, "magicalNightPearl");
	GameRegistry.registerItem(noonPearl, "noonPearl");
	GameRegistry.registerItem(midnightPearl, "midnightPearl");
	GameRegistry.registerItem(dawnPearl, "dawnPearl");
	GameRegistry.registerItem(duskPearl, "duskPearl");
	GameRegistry.registerItem(needle, "needle");
	GameRegistry.registerItem(needleWithThread, "needleWithThread");
	GameRegistry.registerItem(needleWithLinearThread, "needleWithLThread");
	GameRegistry.registerItem(linearThread, "linearString");
	GameRegistry.registerItem(magicalCarpetTemplate, "carpetTemplate");
	GameRegistry.registerItem(woolCarpetTemplate, "matTemplate");
	GameRegistry.registerItem(aspectForma, "aspectForma");
	GameRegistry.registerItem(aspectTime, "aspectTime");
    GameRegistry.registerItem(aspectCharge,"aspectCharge");
    GameRegistry.registerItem(aspectRain, "aspectRain");
    GameRegistry.registerItem(aspectDay, "aspectDay");
    GameRegistry.registerItem(aspectNight,"aspectNight");
    GameRegistry.registerItem(aspectCharm,"aspectCharm");
    GameRegistry.registerItem(aspectSky,"aspectSky");
    GameRegistry.registerItem(aspectHome,"aspectHome");
    GameRegistry.registerItem(aspectHell,"aspectHell");
    GameRegistry.registerItem(aspectEarth,"aspectEarth");
    GameRegistry.registerItem(aspectDeath,"aspectDeath");
    GameRegistry.registerItem(aspectVoice,"aspectVoice");
	
	weaving = new Achievement("weaving", "weaving", 0, 0, needle, (Achievement)null).initIndependentStat().registerStat();
	carpetCreation = new Achievement("carpet", "carpet", 2, 0, MainMod.carpetFormer, weaving).registerStat();
	linearMastery = new Achievement("linear", "linear", 4, 0, MainMod.magicalDayPearl, carpetCreation).registerStat();
	woolenMastery = new Achievement("woollen","woollen", 4, 2, MainMod.carpetFormerFilledWool, carpetCreation).registerStat();
	noLongerOfftopic = new Achievement("chaz","chaz", 0, -5, Items.wheat, (Achievement)null).setSpecial().initIndependentStat().registerStat();
	
    weatherCarpetsPage = new AchievementPage("Weather Carpets", new Achievement[] {weaving, carpetCreation, linearMastery, woolenMastery,noLongerOfftopic});
	
	}
	
	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		
		OreDictionary.registerOre("dyeWhite", bonemealBlock);

		MinecraftForge.EVENT_BUS.register(new LivingMagicalMurderEvent());
		FMLCommonHandler.instance().bus().register(new CraftingHandler());
        FMLCommonHandler.instance().bus().register(new AspectEvents());
		
		GameRegistry.registerTileEntity(TileEntityRainCarpet.class, "anchorman:rainCarpet");
		GameRegistry.registerTileEntity(TileEntityThunderCarpet.class, "anchorman:thunderCarpet");
		GameRegistry.registerTileEntity(TileEntityNoonCarpet.class, "anchorman:noonCarpet");
		GameRegistry.registerTileEntity(TileEntityMidnightCarpet.class, "anchorman:midnightCarpet");
		GameRegistry.registerTileEntity(TileEntityDawnCarpet.class, "anchorman:dawnCarpet");
		GameRegistry.registerTileEntity(TileEntityDuskCarpet.class, "anchorman:duskCarpet");
		GameRegistry.registerTileEntity(TileEntitySpawnCarpet.class, "anchorman:spawnCarpet");
		GameRegistry.registerTileEntity(TileEntityLaunchCarpet.class, "anchorman:launchCarpet");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet.class, "anchorman:welcomeCarpet");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet1.class, "anchorman:welcomeCarpet1");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet2.class, "anchorman:welcomeCarpet2");
		GameRegistry.registerTileEntity(TileEntityWelcomeCarpet3.class, "anchorman:welcomeCarpet3");
		GameRegistry.registerTileEntity(TileEntityExpCarpet.class, "anchorman:expCarpet");
		GameRegistry.registerTileEntity(TileEntityFarmCarpet.class, "anchorman:farmCarpet");
		GameRegistry.registerTileEntity(TileEntityNetherCarpet.class, "anchorman:netherCarpet");
		GameRegistry.registerTileEntity(TileEntityCarpetFormer.class, "anchorman:carpetFormerTE");
		GameRegistry.registerTileEntity(TileEntityCarpetFormerFilled.class, "anchorman:carpetFormerFilledTE");
		GameRegistry.registerTileEntity(TileEntityCarpetFormerFilledWool.class, "anchorman:carpetFormerFilledWoolTE");
        GameRegistry.registerTileEntity(TileEntityMiningCarpet.class, "anchorman:miningCarpet");
        GameRegistry.registerTileEntity(TileEntityTrappedCarpet.class, "anchorman:trappedCarpet");
        GameRegistry.registerTileEntity(TileEntityDetectionCarpet.class, "anchorman:detectionCarpet");
		
		proxy.registerTileEntities();		
		
		GameRegistry.addSmelting(magicalPearl, new ItemStack(magicalPearl2, 1), 0.25F);
		GameRegistry.addRecipe(new ItemStack(magicalWool2, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalPearl2,Character.valueOf('Y'), magicalWool);
//		GameRegistry.addRecipe(new ItemStack(rainSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Items.water_bucket);
//		GameRegistry.addRecipe(new ItemStack(thunderSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Blocks.iron_block);
		GameRegistry.addRecipe(new ItemStack(magicalDayWool , 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayPearl);
		GameRegistry.addRecipe(new ItemStack(magicalNightWool , 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalNightPearl);
//		GameRegistry.addRecipe(new ItemStack(dawnSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool);
//		GameRegistry.addRecipe(new ItemStack(duskSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalNightWool);
//		GameRegistry.addRecipe(new ItemStack(noonSummoner, 1), "XYX", "XXX", "XZX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool, Character.valueOf('Z'), magicalNightWool);
//		GameRegistry.addRecipe(new ItemStack(midnightSummoner, 1), "XZX", "XXX", "XYX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool, Character.valueOf('Z'), magicalNightWool);
//		GameRegistry.addRecipe(new ItemStack(spawnSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Items.bed);
//		GameRegistry.addRecipe(new ItemStack(launcherCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Blocks.tnt);
//		GameRegistry.addRecipe(new ItemStack(welcomeMat, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), new ItemStack(Blocks.wool, 1, 12), Character.valueOf('Y'), Items.sign);
//		GameRegistry.addRecipe(new ItemStack(expCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Blocks.bookshelf);
		if (Loader.isModLoaded("computils")) {
            GameRegistry.addRecipe(new ItemStack(bonemealBlock, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), new ItemStack(Items.dye, 1, 15), 'Y', Items.string);
        }
        else {
            GameRegistry.addRecipe(new ItemStack(bonemealBlock,1),"XXX","XXX","XXX",'X',new ItemStack(Items.dye,1,15));
        }
//		GameRegistry.addRecipe(new ItemStack(farmCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), bonemealBlock);
//		GameRegistry.addRecipe(new ItemStack(netherCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Items.blaze_powder);
		GameRegistry.addRecipe(new ItemStack(needle, 1), "X  "," Y ","  Y", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot);
		GameRegistry.addRecipe(new ItemStack(needle, 1), "Y  "," Y ","  X", Character.valueOf('X'), Items.leather, Character.valueOf('Y'), Items.iron_ingot);
		GameRegistry.addRecipe(new ItemStack(noonPearl, 1), "XXX", "YXY", "XXX", Character.valueOf('X'), MainMod.magicalDayPearl, Character.valueOf('Y'), MainMod.magicalPearl2);
		GameRegistry.addRecipe(new ItemStack(midnightPearl, 1), "XXX", "YXY", "XXX", Character.valueOf('X'), MainMod.magicalNightPearl, Character.valueOf('Y'), MainMod.magicalPearl2);
		GameRegistry.addRecipe(new ItemStack(dawnPearl, 1), "XXX", "YXY", "ZZZ", Character.valueOf('X'), MainMod.magicalDayPearl, Character.valueOf('Y'), MainMod.magicalPearl2, Character.valueOf('Z'), MainMod.magicalNightPearl);
		GameRegistry.addRecipe(new ItemStack(duskPearl, 1), "XXX", "YZY", "ZZZ", Character.valueOf('X'), MainMod.magicalDayPearl, Character.valueOf('Y'), MainMod.magicalPearl2, Character.valueOf('Z'), MainMod.magicalNightPearl);
		GameRegistry.addRecipe(new ItemStack(carpetFormer, 1), "XZX", "XYX", "X X", Character.valueOf('X'), new ItemStack(Blocks.planks, 1, 0), Character.valueOf('Y'), new ItemStack(Blocks.planks, 1, 1), Character.valueOf('Z'), MainMod.magicalPearl2);
		GameRegistry.addRecipe(new ItemStack(linearThread, 1), " Z ", "AXA", " Y ", Character.valueOf('X'), Items.string, Character.valueOf('Y'), Blocks.end_stone, Character.valueOf('Z'), Blocks.grass, Character.valueOf('A'), Blocks.netherrack);
		GameRegistry.addRecipe(new ItemStack(magicalCarpetTemplate, 1), "XYX", "YZY", "XYX", Character.valueOf('X'), MainMod.magicalWool2, Character.valueOf('Y'), Items.string, Character.valueOf('Z'), Items.leather);
		GameRegistry.addRecipe(new ItemStack(woolCarpetTemplate, 1), "XYX","YZY","XYX", Character.valueOf('X'), new ItemStack(Blocks.wool, 1, 12), Character.valueOf('Y'), Items.string, Character.valueOf('Z'),Items.leather);

        GameRegistry.addRecipe(new ItemStack(magicalWool, 1), "XX","XX",'X', MainMod.magicalPearl);
        GameRegistry.addRecipe(new ItemStack(noonWool, 1), "XX", "XX", Character.valueOf('X'), MainMod.noonPearl);
		GameRegistry.addRecipe(new ItemStack(midnightWool, 1), "XX", "XX", Character.valueOf('X'), MainMod.midnightPearl);
		GameRegistry.addRecipe(new ItemStack(dawnWool, 1), "XX", "XX", Character.valueOf('X'), MainMod.dawnPearl);
		GameRegistry.addRecipe(new ItemStack(duskWool, 1), "XX", "XX", Character.valueOf('X'), MainMod.duskPearl);
 
		GameRegistry.addShapelessRecipe(new ItemStack(needleWithThread, 1), MainMod.needle, Items.string);
		GameRegistry.addShapelessRecipe(new ItemStack(needleWithLinearThread, 1), MainMod.needle, MainMod.linearThread);
		GameRegistry.addShapelessRecipe(new ItemStack(Items.dye, 9, 15), bonemealBlock);
		
		DispenserBehaviorAddition.addDispenserBehavior(MainMod.magicalCarpetTemplate, new DispenserBehaviorWool());
		DispenserBehaviorAddition.addDispenserBehavior(MainMod.woolCarpetTemplate, new DispenserBehaviorWool2());
		
		AchievementPage.registerAchievementPage(weatherCarpetsPage);

        FormerRecipeRegistry.registerRecipe(new ItemStack(rainSummoner,1),new ItemStack(Items.water_bucket,1),"Caelum",2,"Pluviae",3,"",69);
        FormerRecipeRegistry.registerRecipe(new ItemStack(thunderSummoner,1),new ItemStack(Blocks.iron_block,1),"Caelum",5,"Pluvaie",6,"",69);
        FormerRecipeRegistry.registerRecipe(new ItemStack(spawnSummoner,1),new ItemStack(Items.bed,1),"Amuletum",6,"Domum",6,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(launcherCarpet,1),new ItemStack(Blocks.tnt,1),"Caelum",3,"Inpero",6,"",0);
		FormerRecipeRegistry.registerRecipe(new ItemStack(expCarpet,1),new ItemStack(Blocks.bookshelf,1),"Amuletum",10,"Forma",4,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(farmCarpet),new ItemStack(bonemealBlock),"Domum",7,"Forma",7,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(netherCarpet),new ItemStack(Items.blaze_powder),"Amuletum",5,"Infernus",10,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(miningCarpet),new ItemStack(Items.diamond_pickaxe),"Terra",7,"Forma",4,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(trappedCarpet),new ItemStack(Blocks.tripwire_hook),"Mortem",6,"Inpero",6,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(detectionCarpet),new ItemStack(Blocks.wooden_pressure_plate),"Inpero",7,"",0,"",0);
        FormerRecipeRegistry.registerRecipe(new ItemStack(noonSummoner), new ItemStack(noonWool),"Lux",6,"Aetas",6,"",0,true);
        FormerRecipeRegistry.registerRecipe(new ItemStack(midnightSummoner), new ItemStack(midnightWool),"Nox",6,"Aetas",6,"",0,true);
        FormerRecipeRegistry.registerRecipe(new ItemStack(dawnSummoner), new ItemStack(dawnWool),"Lux",6,"Nox",4,"Aetas",6,true);
        FormerRecipeRegistry.registerRecipe(new ItemStack(duskSummoner), new ItemStack(duskWool),"Lux",4,"Nox",6,"Aetas",6,true);
        FormerRecipeRegistry.registerRecipe(new ItemStack(welcomeMat), new ItemStack(Items.name_tag),"Vox",6,"Domum",5,"",0,false,true);


    }

    @EventHandler
    public void configChangesInEffect(FMLServerStartedEvent event) {
        if (MainMod.legacyRecipes == true) {
            GameRegistry.addRecipe(new ItemStack(rainSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Items.water_bucket);
            GameRegistry.addRecipe(new ItemStack(thunderSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Blocks.iron_block);

            GameRegistry.addRecipe(new ItemStack(spawnSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Items.bed);
            GameRegistry.addRecipe(new ItemStack(launcherCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Blocks.tnt);

            GameRegistry.addRecipe(new ItemStack(expCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Blocks.bookshelf);

            GameRegistry.addRecipe(new ItemStack(farmCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), bonemealBlock);
            GameRegistry.addRecipe(new ItemStack(netherCarpet, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), Items.blaze_powder);

            GameRegistry.addRecipe(new ItemStack(dawnSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool);
            GameRegistry.addRecipe(new ItemStack(duskSummoner, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalNightWool);

            GameRegistry.addRecipe(new ItemStack(noonSummoner, 1), "XYX", "XXX", "XZX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool, Character.valueOf('Z'), magicalNightWool);
            GameRegistry.addRecipe(new ItemStack(midnightSummoner, 1), "XZX", "XXX", "XYX", Character.valueOf('X'), magicalWool2, Character.valueOf('Y'), magicalDayWool, Character.valueOf('Z'), magicalNightWool);

            GameRegistry.addRecipe(new ItemStack(welcomeMat, 1), "XXX", "XYX", "XXX", Character.valueOf('X'), new ItemStack(Blocks.wool, 1, 12), Character.valueOf('Y'), Items.sign);
        }

        RSHandler.rotation = 1;

    }

    public static void syncConfig() {
        config.addCustomCategoryComment(Configuration.CATEGORY_GENERAL, "These are values for the new AutoUpdate Checker and the new logged in messages. Personally, I would turn stopBuggingMe to true almost immediately! ;P");

        MainMod.versionChannel = config.get(Configuration.CATEGORY_GENERAL, "versionChannel", 0).getInt();
        MainMod.over9000Enabled = config.get(Configuration.CATEGORY_GENERAL, "enableOver9000", 1).getInt();
        MainMod.legacyRecipes = config.get(Configuration.CATEGORY_GENERAL, "enableLegacyCarpetRecipes", false).getBoolean(false);
        MainMod.notifyAll = config.get(Configuration.CATEGORY_GENERAL, "notifyAll", false).getBoolean(false);

//	weaveAchieve = config.get("doNotTouch", "ss1", 0).getInt();
//	carpetAchieve = config.get("doNotTouch", "ss2", 0).getInt();
//	linearAchieve = config.get("doNotTouch", "ss3", 0).getInt();
        if (config.hasChanged())
            config.save();
    }

    public static String getVersionString() {
        String name = "";
        switch (versionType) {
            case 0: name = "weatherCarpet"; break;
            case 1: name = "weatherCarpetDev"; break;
            case 2: name = "weatherCarpetBeta"; break;
            default: break;
        }
        return name;
    }

}
