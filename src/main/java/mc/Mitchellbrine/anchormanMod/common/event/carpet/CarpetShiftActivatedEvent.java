package mc.Mitchellbrine.anchormanMod.common.event.carpet;

import cpw.mods.fml.common.eventhandler.Event;
import mc.Mitchellbrine.anchormanMod.common.block.WeatherCarpet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class CarpetShiftActivatedEvent extends Event{

    public WeatherCarpet carpet;
    public final World world;
    public final EntityPlayer player;

    public CarpetShiftActivatedEvent(WeatherCarpet carpet, EntityPlayer player, World world) {
        this.carpet = carpet;
        this.player = player;
        this.world = world;
    }

}
