package mc.Mitchellbrine.anchormanMod.common.event;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import net.minecraft.item.Item;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;

public class CraftingHandler {

	@SubscribeEvent
	public void craftingStuff(ItemCraftedEvent event) {
		if (!event.player.worldObj.isRemote) {
		if (event.crafting.getItem() == MainMod.needleWithThread) {
			event.player.addStat(MainMod.weaving, 1);
		}
		if (event.crafting.getItem() == Item.getItemFromBlock(MainMod.carpetFormer)) {
			if (event.player.inventory.hasItem(MainMod.needleWithThread)) {
				event.player.addStat(MainMod.carpetCreation, 1);
			}
		}
		if (event.crafting.getItem() == MainMod.needleWithLinearThread) {
			event.player.addStat(MainMod.linearMastery, 1);
		}
		}
	}

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs) {
        if(eventArgs.modID.equals("weatherManMod"))
            MainMod.syncConfig();
    }
	
}
