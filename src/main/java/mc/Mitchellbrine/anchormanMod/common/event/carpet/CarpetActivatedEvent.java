package mc.Mitchellbrine.anchormanMod.common.event.carpet;

import cpw.mods.fml.common.eventhandler.Cancelable;
import cpw.mods.fml.common.eventhandler.Event;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import mc.Mitchellbrine.anchormanMod.common.block.WeatherCarpet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import java.lang.annotation.Annotation;

public class CarpetActivatedEvent extends Event{

    public WeatherCarpet carpet;
    public final World world;
    public final EntityPlayer player;

    public CarpetActivatedEvent(WeatherCarpet carpet, EntityPlayer player, World world) {
        this.carpet = carpet;
        this.player = player;
        this.world = world;
    }
}
