package mc.Mitchellbrine.anchormanMod.common.event;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.util.AutoUpdateChecker;
import mc.Mitchellbrine.anchormanMod.util.CloudChecking;
import net.minecraft.entity.monster.*;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.passive.EntityCow;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.world.BlockEvent;

public class LivingMagicalMurderEvent {
	
	
    public static double rand;

    @SubscribeEvent
    public void onEntityDrop(LivingDropsEvent event) {
                 rand = Math.random();

                 Random random = new Random();
                 
                  if (event.entityLiving instanceof EntityEnderman) {
                       if (rand < 0.5d){
                    	  if (random.nextInt(1) == 0){
                          event.entityLiving.dropItem(MainMod.magicalPearl, random.nextInt(3));
                          }
                    	  else {
                           event.entityLiving.dropItem(Item.getItemFromBlock(MainMod.magicalWool), random.nextInt(3));
                    	  }
                     }

                      if (rand < 0.3d) {
                          event.entityLiving.dropItem(MainMod.aspectRain, random.nextInt(5));
                      }
               }


                if (event.entityLiving instanceof EntityVillager) {
                	  if (rand < 0.5d) {
                		  if (((EntityVillager)event.entityLiving).getProfession() == 2) {
                			  event.entityLiving.dropItem(MainMod.magicalPearl2, random.nextInt(3));
                		  }
                		  else {
                			  event.entityLiving.dropItem(MainMod.magicalDayPearl, random.nextInt(3));    
                		  }
                		  
                		  }

                      if (rand < 0.3d) {
                          event.entityLiving.dropItem(MainMod.aspectVoice, random.nextInt(3));
                      }

                      if (rand < 0.7d) {
                          event.entityLiving.dropItem(MainMod.aspectDay,random.nextInt(2));
                      }

                      if (rand < 0.4d) {
                          event.entityLiving.dropItem(MainMod.aspectHome, random.nextInt(5));
                      }

                  }



                    if (event.entityLiving instanceof EntitySkeleton) {
                	  if (((EntitySkeleton)event.entityLiving).getSkeletonType() == 1) {
                		  if (rand < 0.5d) {
                			  event.entityLiving.dropItem(MainMod.magicalNightPearl, random.nextInt(3));
                		  }
                          if (rand < 0.3d) {
                              event.entityLiving.dropItem(MainMod.aspectHell, random.nextInt(5));
                          }
                	  } else {
                          if (rand < 0.3d) {
                              event.entityLiving.dropItem(MainMod.aspectDeath, random.nextInt(3));
                          }
                      }
                  }

                  if (event.entityLiving instanceof EntityZombie) {
                      if (rand < 0.3d) {
                         event.entityLiving.dropItem(MainMod.aspectNight, random.nextInt(5));
                      }

                      if (rand < 0.7d) {
                          event.entityLiving.dropItem(MainMod.aspectForma, random.nextInt(10));
                      }
                  }

                  if (event.entityLiving instanceof EntityGhast) {
                      if (rand < 0.5d) {
                          event.entityLiving.dropItem(MainMod.aspectSky,random.nextInt(7));
                      }
                      if (rand < 0.3d) {
                          event.entityLiving.dropItem(MainMod.aspectHell, random.nextInt(5));
                      }
                  }

                if (event.entityLiving instanceof EntityBat) {
                    if (rand < 0.5d) {
                    event.entityLiving.dropItem(MainMod.aspectSky,random.nextInt(7));
                    }
                 }

                if (event.entityLiving instanceof EntityChicken) {
                    if (rand < 0.5d) {
                    event.entityLiving.dropItem(MainMod.aspectSky,random.nextInt(7));
                    }
                }

                  if (event.entityLiving instanceof EntityCow) {
                      if (rand < 0.4d) {
                          event.entityLiving.dropItem(MainMod.aspectTime, random.nextInt(5));
                      }
                  }

                  if (event.entityLiving instanceof EntityWitch) {
                      if (rand < 0.3d) {
                          event.entityLiving.dropItem(MainMod.aspectCharm, random.nextInt(5));
                      }
                  }

                  if (event.entityLiving instanceof EntitySilverfish) {
                      if (rand < 0.3d) {
                          event.entityLiving.dropItem(MainMod.aspectEarth, random.nextInt(5));
                      }
                  }
    
    }

    @SubscribeEvent
    public void harvest(BlockEvent.HarvestDropsEvent event) {
        rand = Math.random();

        Random random = new Random();

        if (event.block.equals(Blocks.redstone_ore) || event.block.equals(Blocks.lit_redstone_ore)) {
            if (rand < 0.5d) {
                event.drops.add(new ItemStack(MainMod.aspectCharge,random.nextInt(5)));
            }
        }

    }

    @SubscribeEvent
    public void playerJoinCheck(EntityJoinWorldEvent event) {
    	if (event.entity instanceof EntityPlayer) {
			if (MainMod.versionChannel == 0) {
    		if (event.world.isRemote) {
        			try {
    					AutoUpdateChecker.isUpdateAvailableStable((EntityPlayer)event.entity);
        			} catch (MalformedURLException e) {
    					e.printStackTrace();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
    			}
			}
    	}
    	}
    
    @SubscribeEvent
    public void playerJoinCheckDev(EntityJoinWorldEvent event) {
    	if (event.entity instanceof EntityPlayer) {
			if (MainMod.versionChannel == 1) {
	    		if (event.world.isRemote) {
        			try {
    					AutoUpdateChecker.isUpdateAvailableDev((EntityPlayer)event.entity);
        			} catch (MalformedURLException e) {
    					e.printStackTrace();
    				} catch (IOException e) {
    					e.printStackTrace();
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
        			
    			} 				
			}
		}
    	}
    
    @SubscribeEvent
    public void playerJoinCheckBeta(EntityJoinWorldEvent event) {
    	if (event.entity instanceof EntityPlayer) {
			if (MainMod.versionChannel == 2) {
	    		if (event.world.isRemote) {
        			try {
    					AutoUpdateChecker.isUpdateAvailableBeta((EntityPlayer)event.entity);
        			} catch (MalformedURLException e) {
    					e.printStackTrace();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
    			} 				
			}
		}
    	}

@SubscribeEvent
public void playerValidation(EntityJoinWorldEvent event) {
	if (event.entity instanceof EntityPlayer) {
		if (CloudChecking.isUserRegistered((EntityPlayer)event.entity)) {
    		if (!event.world.isRemote) {
    			MainMod.isUserValidated = 1;
			} 				
		}
	}
	}
    
}
	
