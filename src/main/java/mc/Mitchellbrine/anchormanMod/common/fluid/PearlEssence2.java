package mc.Mitchellbrine.anchormanMod.common.fluid;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class PearlEssence2 extends BlockFluidClassic {

    @SideOnly(Side.CLIENT)
    protected IIcon stillIcon;
    @SideOnly(Side.CLIENT)
    protected IIcon flowingIcon;
    
    public PearlEssence2(Fluid fluid, Material material) {
            super(fluid, material);
            setBlockName("pearlEssenceThick");
    }
    
    @Override
    public IIcon getIcon(int side, int meta){
            return (side == 0 || side == 1)? stillIcon : flowingIcon;
    }
    
    @SideOnly(Side.CLIENT)
    @Override
    public void registerBlockIcons(IIconRegister register) {
            stillIcon = register.registerIcon("anchorman:pearlEssenceThick");
            flowingIcon = register.registerIcon("anchorman:pearlEssenceThickF");
    }
    
    /**
     * Returns true if the block at (x, y, z) is displaceable. Does not displace the block.
     */
    public boolean canDisplace(IBlockAccess world, int x, int y, int z)
    {
        if (world.getBlock(x, y, z).isAir(world, x, y, z)) return true;

        Block block = world.getBlock(x, y, z);

        if (displacements.containsKey(block))
        {
            return displacements.get(block);
        }

        Material material = block.getMaterial();
        if (material.blocksMovement() || material == Material.portal)
        {
            return false;
        }
        return false;
    }

    /**
     * Attempt to displace the block at (x, y, z), return true if it was displaced.
     */
    public boolean displaceIfPossible(World world, int x, int y, int z)
    {
        if (world.getBlock(x, y, z).isAir(world, x, y, z))
        {
            return true;
        }

        Block block = world.getBlock(x, y, z);

        Material material = block.getMaterial();
        if (material.blocksMovement() || material == Material.portal)
        {
            return false;
        }
        return false;
    }

    @Override
    public int getQuantaValue(IBlockAccess world, int x, int y, int z)
    {
        if (world.getBlock(x, y, z).isAir(world, x, y, z))
        {
            return 0;
        }

        if (world.getBlock(x, y, z) != this)
        {
            return -1;
        }

        int quantaRemaining = world.getBlockMetadata(x, y, z) + 1;
        return quantaRemaining;
    }

    @Override
    public boolean canCollideCheck(int meta, boolean fullHit)
    {
        return fullHit && meta == quantaPerBlock - 1;
    }
    
}