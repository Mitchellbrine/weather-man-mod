package mc.Mitchellbrine.anchormanMod.common.fluid;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidContainerRegistry;
import net.minecraftforge.fluids.FluidRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

public class RegisterFluids {

	public static Fluid magicalEssence = new Fluid("pearlEssence").setLuminosity(8).setViscosity(500);
	public static Fluid magicalEssenceThick = new Fluid("pearlEssenceThick").setLuminosity(10).setViscosity(2000);
	public static Block pearlEssence1;
	public static Block pearlEssence2;
	
	public static Item pEBucket = new ItemPearlBucket(pearlEssence1);
	
	public static void registerFluids(){ 
	FluidRegistry.registerFluid(magicalEssence);
	FluidRegistry.registerFluid(magicalEssenceThick);
	
	pearlEssence1 = new PearlEssence(magicalEssence, Material.water);
	pearlEssence2 = new PearlEssence2(magicalEssenceThick, Material.water);

	GameRegistry.registerBlock(pearlEssence1, "pearlEssence");
	GameRegistry.registerBlock(pearlEssence2, "pearlEssenceThick");
	
	magicalEssence.setUnlocalizedName(pearlEssence1.getUnlocalizedName());
	magicalEssenceThick.setUnlocalizedName(pearlEssence2.getUnlocalizedName());
	
	pEBucket.setUnlocalizedName("pEBucket").setContainerItem(Items.bucket);
	GameRegistry.registerItem(pEBucket, "pEBucket");
	
	FluidContainerRegistry.registerFluidContainer(magicalEssence, new ItemStack(pEBucket), new ItemStack(Items.bucket));
	
	BucketHandler.INSTANCE.buckets.put(pearlEssence1, pEBucket);
	MinecraftForge.EVENT_BUS.register(BucketHandler.INSTANCE);
	
	}
	
}
