package mc.Mitchellbrine.anchormanMod.common.fluid;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ItemPearlBucket extends ItemBucket
{

    public ItemPearlBucket(Block p_i45331_1_)
    {
    	super(p_i45331_1_);
        this.setCreativeTab(CreativeTabs.tabMisc);
    }
   
    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
    	if (!par2World.isRemote) {
    		Block block = par2World.getBlock((int)par3EntityPlayer.posX, (int)par3EntityPlayer.posY, (int)par3EntityPlayer.posZ);
    		par2World.setBlock((int)par3EntityPlayer.posX - 1, (int)par3EntityPlayer.posY, (int)par3EntityPlayer.posZ, RegisterFluids.pearlEssence1);
    		if (!par3EntityPlayer.capabilities.isCreativeMode) {
    			par3EntityPlayer.setCurrentItemOrArmor(0, new ItemStack(Items.bucket));
    		}
    	}
    	return par1ItemStack;
    }


}