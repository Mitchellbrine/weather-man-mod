package mc.Mitchellbrine.anchormanMod.common.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityDetectionCarpet extends TileEntity {

    private int setting;

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        if (nbt != null) {
            nbt.setInteger("setting",setting);
        }
        else {
            nbt = new NBTTagCompound();
            nbt.setInteger("setting",setting);
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        if (nbt != null) {
            if (nbt.hasKey("setting")) {
                this.setting = nbt.getInteger("setting");
            }
            else {
                this.setting = 0;
            }
        }
        else {
            nbt = new NBTTagCompound();
            this.setting = 0;
        }
    }

    public int setSetting(int setting) {
        this.setting = setting;
        return this.setting;
    }

    public int getSetting() {
        return this.setting;
    }

}
