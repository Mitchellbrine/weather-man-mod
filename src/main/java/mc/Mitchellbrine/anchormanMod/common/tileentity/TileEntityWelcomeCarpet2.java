package mc.Mitchellbrine.anchormanMod.common.tileentity;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

public class TileEntityWelcomeCarpet2 extends TileEntity {
	
	public String ownerName = "";
	
	
	public void writeToNBT(NBTTagCompound nbt)
	{
		super.writeToNBT(nbt);
		nbt.setString("ownerName", ownerName);
	}
	
	public void readFromNBT(NBTTagCompound nbt)
	{
		super.readFromNBT(nbt);
		if (nbt.hasKey("ownerName"))
		{
			ownerName = nbt.getString("ownerName");
		}
	}
	

}
