package mc.Mitchellbrine.anchormanMod.common.block;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityCarpetFormer;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class CarpetFormer extends BlockContainer {
	
	public CarpetFormer() {
		super(Material.wood);
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityCarpetFormer();
	}
	
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
		if (!par1World.isRemote){
		if (par5EntityPlayer.inventory.getCurrentItem() != null){
			if (par5EntityPlayer.inventory.getCurrentItem().getItem() == MainMod.magicalCarpetTemplate) {
				par1World.setBlock(par2, par3, par4, MainMod.carpetFormerFilled);
				if (!par5EntityPlayer.capabilities.isCreativeMode){
				par5EntityPlayer.inventory.consumeInventoryItem(MainMod.magicalCarpetTemplate);
		}
			}
			else if (par5EntityPlayer.inventory.getCurrentItem().getItem() == MainMod.woolCarpetTemplate) {
					par1World.setBlock(par2, par3, par4, MainMod.carpetFormerFilledWool);
					if (!par5EntityPlayer.capabilities.isCreativeMode){
					par5EntityPlayer.inventory.consumeInventoryItem(MainMod.woolCarpetTemplate);
					}

				}
		}
		return true;
		}
		return false;
		
	}
	
	public boolean isFlammable(IBlockAccess par1BlockAccess, int x, int y, int z, ForgeDirection direction) {
		return true;
	}

}
