package mc.Mitchellbrine.anchormanMod.common.block;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import mc.Mitchellbrine.anchormanMod.aspects.Aspect;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.event.carpet.CarpetActivatedEvent;
import mc.Mitchellbrine.anchormanMod.common.event.carpet.CarpetShiftActivatedEvent;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

public abstract class WeatherCarpet extends BlockContainer{

    public WeatherCarpet(Aspect aspect1, Aspect aspect2, Aspect aspect3) {
        super(Material.cloth);
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
        MainMod.aspects1.put(this,aspect1);
        if (aspect2 != null) {
            MainMod.aspects2.put(this,aspect2);
        }
        if (aspect3 != null) {
            MainMod.aspects3.put(this,aspect3);
        }
    }

    public WeatherCarpet(Material material,Aspect aspect1, Aspect aspect2, Aspect aspect3) {
        super(material);
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
        MainMod.aspects1.put(this,aspect1);
        if (aspect2 != null) {
            MainMod.aspects2.put(this,aspect2);
        }
        if (aspect3 != null) {
            MainMod.aspects3.put(this,aspect3);
        }
    }


    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }

    public boolean onBlockActivated(World par1World, int x, int y, int z, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        if (!par1World.isRemote) {
            if (!par5EntityPlayer.isSneaking()) {
                MinecraftForge.EVENT_BUS.post(new CarpetActivatedEvent((WeatherCarpet) par1World.getBlock(x, y, z), par5EntityPlayer, par1World));
            }
            else {
                MinecraftForge.EVENT_BUS.post(new CarpetShiftActivatedEvent((WeatherCarpet)par1World.getBlock(x,y,z),par5EntityPlayer,par1World));
            }
        }
        return true;
    }



}
