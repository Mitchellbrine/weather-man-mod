package mc.Mitchellbrine.anchormanMod.common.block;

import java.io.IOException;
import java.net.MalformedURLException;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityLaunchCarpet;
import mc.Mitchellbrine.anchormanMod.util.CloudChecking;
import mc.Mitchellbrine.anchormanMod.util.RegisteredUsersChecker;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class LaunchCarpet extends WeatherCarpet {



	public LaunchCarpet() {
        super(MainMod.aspectSky, MainMod.aspectCharge,null);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityLaunchCarpet();
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
        TileEntityLaunchCarpet te = (TileEntityLaunchCarpet) par1World.getTileEntity(par2, par3, par4);

        if (!par5EntityPlayer.isSneaking()) {
            par1World.getClosestPlayer(par2, par3, par4, 10).motionY += te.power;

            if (!par1World.isRemote) {
                if (te != null && te.power == 2.0) {
                    par1World.playSoundAtEntity(par1World.getClosestPlayer(par2, par3, par4, 10), "anchorman:space", 10.0F, 1.0F);
                }
                par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Launch in progress!"));
                return true;
            }
            } else {
            if (!par1World.isRemote) {
                if (te.power == 1.5) {
                    te.power = 2.0;
                    par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: HIGH"));
                } else if (te.power == 1.0) {
                    te.power = 1.5;
                    par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: MEDIUM"));
                } else if (te.power == 2.0) {
                    MainMod.notified++;
                    RegisteredUsersChecker.isUserValidated(par5EntityPlayer);
                    if (MainMod.over9000Enabled == 1) {
                        te.power = 9000000;
                        par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: " + EnumChatFormatting.DARK_RED + "" + EnumChatFormatting.ITALIC + "OVER 9000!"));
                    } else {
                        te.power = 1.0;
                        par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: LOW"));
                    }
                } else if (te.power == 9000000) {
                    te.power = 1.0;
                    par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "" + EnumChatFormatting.ITALIC + "Power set to: LOW"));
                }
            }
    	}

    	
    	return false;
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
//    public void registerIcons(IIconRegister icon) {
//            this.blockIcon = icon.registerIcon(MainMod.MOD_ID + ":" + "lIcon");
//    }

	

}
