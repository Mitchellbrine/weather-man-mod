package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.List;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityRainCarpet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;

public class RainSummoner extends WeatherCarpet {

	public RainSummoner() {
        super(MainMod.aspectSky,MainMod.aspectRain,null);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityRainCarpet();
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    //This is the icon to use for showing the block in your hand.
//    public void registerIcons(IIconRegister icon) {
//            this.blockIcon = icon.registerIcon(MainMod.MOD_ID + ":" + "rSIcon");
//    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	if (!par1World.isRemote)
    	{
        WorldServer worldserver = MinecraftServer.getServer().worldServers[par5EntityPlayer.dimension];
        WorldInfo worldinfo = worldserver.getWorldInfo();
        
        if (par5EntityPlayer.isSneaking() && worldinfo.getRainTime() > 0)
        {
		worldinfo.setRaining(true);
		par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Rain has been summoned!"));
		
		CarpetUtilities.broadcastMessage(par1World, par5EntityPlayer.getCommandSenderName() + " has summoned rain!");
		
        }
        else
        {
        	worldinfo.setRaining(false);
    		par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Rain has been banished!"));
    		
    		CarpetUtilities.broadcastMessage(par1World, par5EntityPlayer.getCommandSenderName() + " has banished the rain!");
    		
        }
    	}
    	
    	return false;
    }
    
	

}
