package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityFarmCarpet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class FarmCarpet extends WeatherCarpet {


	public FarmCarpet() {
		super(MainMod.aspectHome, MainMod.aspectForma,null);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileEntityFarmCarpet();
	}
	
	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityFarmCarpet();
	}
	
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public static boolean isRequiredSeeds(EntityPlayer player) {
    	if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() == Items.wheat_seeds || player.getCurrentEquippedItem().getItem() == Items.melon_seeds || player.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds || player.getCurrentEquippedItem().getItem() == Items.carrot || player.getCurrentEquippedItem().getItem() == Items.potato) {
    		return true;
    	}
    	else if (player.getCurrentEquippedItem() == null && player.inventory.hasItem(Items.wheat_seeds) || player.inventory.hasItem(Items.melon_seeds) || player.inventory.hasItem(Items.pumpkin_seeds) || player.inventory.hasItem(Items.carrot) || player.inventory.hasItem(Items.potato)) {
    		return true;
    	}
    	else {
    	return false;
    }
    }
    
    public static boolean isRequiredSeedsClient(EntityClientPlayerMP player) {
    	if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() == Items.wheat_seeds || player.getCurrentEquippedItem().getItem() == Items.melon_seeds || player.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds || player.getCurrentEquippedItem().getItem() == Items.carrot || player.getCurrentEquippedItem().getItem() == Items.potato) {
    		return true;
    	}
    	else if (player.getCurrentEquippedItem() == null && player.inventory.hasItem(Items.wheat_seeds) || player.inventory.hasItem(Items.melon_seeds) || player.inventory.hasItem(Items.pumpkin_seeds) || player.inventory.hasItem(Items.carrot) || player.inventory.hasItem(Items.potato)) {
    		return true;
    	}
    	else {
    	return false;
    }
    }
    
    public boolean onBlockActivated(World par1World, int x1, int y1, int z1, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	if (!par1World.isRemote)
    	{    		
    		if (!par5EntityPlayer.isSneaking())
    		{
    			if ((par5EntityPlayer.inventory.hasItem(Items.wheat_seeds) || par5EntityPlayer.inventory.hasItem(Items.pumpkin_seeds) || par5EntityPlayer.inventory.hasItem(Items.melon_seeds) || par5EntityPlayer.inventory.hasItem(Items.carrot) || par5EntityPlayer.inventory.hasItem(Items.potato)))
    			{
    				if (par5EntityPlayer.inventory.hasItem(Items.wheat_seeds) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.wheat_seeds)
    				{
           				EntityPlayerMP entityplayermp = (EntityPlayerMP)par5EntityPlayer;
           				if (par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.wheat_seeds) {
    					if (!entityplayermp.func_147099_x().hasAchievementUnlocked(MainMod.noLongerOfftopic)) {
    						par5EntityPlayer.addStat(MainMod.noLongerOfftopic, 1);
    					}
           				}
        				par5EntityPlayer.inventory.consumeInventoryItem(Items.wheat_seeds);
        				par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Items.wheat, 1));
           				par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Wheat grown!"));

    				}
    				else if (par5EntityPlayer.inventory.hasItem(Items.pumpkin_seeds) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds)
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Items.pumpkin_seeds);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Blocks.pumpkin, 1));
    					par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Pumpkin grown!"));
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Items.melon_seeds) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.melon_seeds)
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Items.melon_seeds);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Blocks.melon_block, 1));
    					par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Melon grown!"));
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Items.carrot) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.carrot)
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Items.carrot, 1));
    					par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Carrot grown!"));
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Items.potato) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.potato)
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Items.potato, 1));
    					par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Potato grown!"));
    				}
        			return true;
    			}
    			else
    			{
                    if (par5EntityPlayer.getCurrentEquippedItem() != null && par5EntityPlayer.getCurrentEquippedItem().getItem().getUnlocalizedName().contains("hoe")) {
                        if (par5EntityPlayer.getCurrentEquippedItem().getItemDamage() <= par5EntityPlayer.getCurrentEquippedItem().getMaxDamage() - 4) {
                            for (int x = x1-1; x < x1 + 2; x++) {
                                for (int z = z1-1; z < z1 + 2; z++) {
                                    System.err.println(x + " " + (y1 - 1) + " " + z);
                                    if (par1World.getBlock(x,y1-1,z) == Blocks.dirt || par1World.getBlock(x,y1-1,z) == Blocks.grass) {
                                        par1World.setBlock(x, y1 - 1, z, Blocks.farmland);
                                    }
                                }
                            }
                        }
                    }
                        par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Seeds required for farming!"));
    			}
    		}
            else {

            }
    	}
    	else
    	{    		
    		if (!par5EntityPlayer.isSneaking())
    		{
    			if ((par5EntityPlayer.inventory.hasItem(Items.wheat_seeds) || par5EntityPlayer.inventory.hasItem(Items.pumpkin_seeds) || par5EntityPlayer.inventory.hasItem(Items.melon_seeds) || par5EntityPlayer.inventory.hasItem(Items.carrot) || par5EntityPlayer.inventory.hasItem(Items.potato)))
    			{
    				
    				if (par5EntityPlayer.inventory.hasItem(Items.wheat_seeds) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.wheat_seeds)
    				{
    				par5EntityPlayer.inventory.consumeInventoryItem(Items.wheat_seeds);
    				par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Items.wheat, 1));
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Items.pumpkin_seeds) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds)
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Items.pumpkin_seeds);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Blocks.pumpkin, 1));
    				}
    				else if (par5EntityPlayer.inventory.hasItem(Items.melon_seeds) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.melon_seeds)
    				{
    					par5EntityPlayer.inventory.consumeInventoryItem(Items.melon_seeds);
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Blocks.melon_block, 1));
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Items.carrot) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.carrot)
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Items.carrot, 1));
    				}
    				
    				if (par5EntityPlayer.inventory.hasItem(Items.potato) || par5EntityPlayer.getCurrentEquippedItem().getItem() == Items.potato)
    				{
    					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Items.potato, 1));
    				}

    				return true;
    			}
    		}    		
    	}
    	return false;
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
//    public void registerIcons(IIconRegister icon) {
 //           this.blockIcon = icon.registerIcon(MainMod.MOD_ID + ":" + "fIcon");
 //   }
    
    public int idDropped(int par1, Random par2Random, int par3)
    {
		return blockRegistry.getIDForObject(MainMod.farmCarpet);
    	
    }
	

}
