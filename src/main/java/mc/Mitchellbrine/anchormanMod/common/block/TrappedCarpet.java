package mc.Mitchellbrine.anchormanMod.common.block;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityTrappedCarpet;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.Random;

public class TrappedCarpet extends WeatherCarpet{

    public TrappedCarpet() {
        super(MainMod.aspectDeath,MainMod.aspectCharge,null);
        this.setTickRandomly(true);
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new TileEntityTrappedCarpet();
    }

    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int meta, float hitX, float hitY, float hitZ) {
        world.createExplosion(player,x,y+1,z,7.0F,false);
        player.motionY += 1;
        for (int x1 = x - 1; x1 < x + 2; x1++) {
            for (int y1 = y - 20; y1 < y + 1; y1++) {
                for (int z1 = z - 1; z1 < z + 2;z1++) {
                    if ((world.getBlock(x1,y1,z1).getBlockHardness(world,x1,y1,z1) < 50 || world.getBlock(x1,y1,z1) != Blocks.bedrock)) {
                        world.func_147480_a(x1, y1, z1, true);
                    }
                }
            }
        }

        return true;
    }

    public void updateTick(World world, int x, int y, int z,Random random) {
        if (world.isBlockIndirectlyGettingPowered(x,y,z)) {
            world.createExplosion(world.getClosestPlayer(x,y,z,50),x,y,z,5.0F,true);
        }
    }
}
