package mc.Mitchellbrine.anchormanMod.common.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDispenser;
import net.minecraft.dispenser.IBehaviorDispenseItem;
import net.minecraft.item.Item;

public class DispenserBehaviorAddition {

    public static void addDispenserBehavior(Item item, IBehaviorDispenseItem behavior)
    {
        BlockDispenser.dispenseBehaviorRegistry.putObject(item, behavior);
    }
    
    public static void addDispenserBehavior(Block block, IBehaviorDispenseItem behavior) {
    	BlockDispenser.dispenseBehaviorRegistry.putObject(block, behavior);
    }
	
}
