package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.aspects.AspectHelper;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityCarpetFormerFilledWool;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class CarpetFormerFilledWool extends BlockContainer {
		
    private ItemStack needleWithThread = new ItemStack(MainMod.needleWithThread);
    private ItemStack needleWithLinearThread = new ItemStack(MainMod.needleWithLinearThread);
	
	public CarpetFormerFilledWool() {
		super(Material.cloth);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0625F, 1.0F);
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityCarpetFormerFilledWool();
	}
	
	
	
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
			ItemStack currentItem = par5EntityPlayer.inventory.getCurrentItem();
		
			if (!par1World.isRemote) {
			EntityPlayerMP entityplayermp = (EntityPlayerMP)par5EntityPlayer;
			if (!par5EntityPlayer.isSneaking()) {
				if (par5EntityPlayer.getCurrentEquippedItem() != null) {
					if (entityplayermp.func_147099_x().hasAchievementUnlocked(MainMod.woolenMastery)) {
						if (currentItem.getItem() == Items.name_tag && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectVox",6,"aspectDomum",5)) {
							par5EntityPlayer.inventory.consumeInventoryItem(Items.name_tag);
							par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.welcomeMat), 1));
							par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
						}
				}
				else {
					par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "This process is too complicated for you..."));
					return false;
					}
			}
				else {
					par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(MainMod.woolCarpetTemplate, 1));
					par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
				}
			}
			return true;
		}
			else {
				EntityClientPlayerMP entityplayermp = (EntityClientPlayerMP)par5EntityPlayer;
				if (!par5EntityPlayer.isSneaking()) {
					if (par5EntityPlayer.getCurrentEquippedItem() != null) {
						if (entityplayermp.getStatFileWriter().hasAchievementUnlocked(MainMod.woolenMastery)) {
							if (currentItem.getItem() == Items.name_tag && inventoryContainsNeedle(par5EntityPlayer)) {
								par5EntityPlayer.inventory.consumeInventoryItem(Items.name_tag);
								par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(MainMod.welcomeMat, 1));
								par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
							}
					}
					else {
						return false;
						}
						
				}
					else {
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(MainMod.woolCarpetTemplate, 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
					}
				}
				return true;
			}
	}
	
	private boolean inventoryContainsNeedle(EntityPlayer par5EntityPlayer) {
		if (par5EntityPlayer.inventory.hasItem(MainMod.needleWithThread))
		{
			return true;
		}
		return false;
	}
	
	private boolean inventoryContainsNeedleLinear(EntityPlayer par5EntityPlayer) {
		if (par5EntityPlayer.inventory.hasItem(MainMod.needleWithLinearThread))
		{
			return true;
		}
		return false;
	}

	public boolean isFlammable(IBlockAccess par1BlockAccess, int x, int y, int z, ForgeDirection direction) {
		return true;
	}
	
    public int idDropped(int par1, Random par2Random, int par3)
    {
		return blockRegistry.getIDForObject(MainMod.carpetFormer);
    	
    }
    
    private boolean hasLinearMastery(EntityPlayerMP par1EntityPlayer){
    	if (par1EntityPlayer.func_147099_x().hasAchievementUnlocked(MainMod.linearMastery)) {
    		return true;
    	}
    	return false;
    }
    
    private boolean hasLinearMasteryClient(EntityClientPlayerMP par1EntityPlayer) {
    	if (par1EntityPlayer.getStatFileWriter().hasAchievementUnlocked(MainMod.linearMastery)) {
    		return true;
    	}
    	return false;
    }
   
}
