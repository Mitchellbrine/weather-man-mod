package mc.Mitchellbrine.anchormanMod.common.block;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityMiningCarpet;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityNetherCarpet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

import java.util.Random;

public class MiningCarpet extends WeatherCarpet{

    public MiningCarpet() {
        super(MainMod.aspectEarth,MainMod.aspectForma, null);
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int par2) {
        return new TileEntityMiningCarpet();
    }

    public boolean onBlockActivated(World par1World, int x, int y, int z, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {

        if (!par1World.isRemote)
        {
            if (!par5EntityPlayer.isSneaking()) {
                if (par5EntityPlayer.getCurrentEquippedItem() != null && par5EntityPlayer.getCurrentEquippedItem().getItem().getUnlocalizedName().contains("pickaxe") && par5EntityPlayer.getCurrentEquippedItem().getItem().getHarvestLevel(par5EntityPlayer.getCurrentEquippedItem(),"pickaxe") > 1) {
                    if (par5EntityPlayer.getCurrentEquippedItem().getItemDamage() <= par5EntityPlayer.getCurrentEquippedItem().getMaxDamage() - 13) {

                        int damageFromUse = 0;
                        Random random = new Random();

                        /*//Bottom Row

                        if (par1World.getBlock(x-1,y-1,z-1).getBlockHardness(par1World,x-1,y-1,z-1) > && par1World.func_147480_a(x-1,y-1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x-1,y-1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x-1,y-1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y-1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y-1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y-1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y-1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y-1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y-1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}

                        // Middle Row

                        if (par1World.func_147480_a(x-1,y,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x-1,y,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x-1,y,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}

                        // Top Row

                        if (par1World.func_147480_a(x-1,y+1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x-1,y+1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x-1,y+1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y+1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y+1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y+1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y+1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x+1,y+1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (par1World.func_147480_a(x,y+1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}

                        par5EntityPlayer.getCurrentEquippedItem().damageItem(damageFromUse,par5EntityPlayer); */

                        for (int x1 = x - 1; x1 < x + 2;x1++) {

                            for (int y1 = y - 1; y1 < y + 2;y1++) {

                                for (int z1 = z - 1; z1 < z + 2;z1++) {

                                    if ((par1World.getBlock(x1,y1,z1).getBlockHardness(par1World,x1,y1,z1) < 50 || par1World.getBlock(x1,y1,z1) != Blocks.bedrock) && par1World.getBlock(x1,y1,z1) != par1World.getBlock(x,y,z)) {
                                        par1World.func_147480_a(x1,y1,z1,true);
                                        if (random.nextInt(100) > 50) {
                                            par5EntityPlayer.getCurrentEquippedItem().damageItem(1,par5EntityPlayer);
                                        }
                                    }

                                }

                            }

                        }

                    }
                }
                else if (par5EntityPlayer.getCurrentEquippedItem() == null) {
                    par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "" + EnumChatFormatting.ITALIC + "Pickaxe needed!"));
                } else {
                    par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "" + EnumChatFormatting.ITALIC + "Pickaxe needed!"));
                }
            }
        }

        return false;
    }

    @Override
    public int getRenderType() {
        return -1;
    }

    @Override
    public boolean isOpaqueCube() {
        return false;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }

}
