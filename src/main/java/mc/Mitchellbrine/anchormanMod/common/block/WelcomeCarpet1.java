package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityWelcomeCarpet1;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class WelcomeCarpet1 extends WeatherCarpet {



	public WelcomeCarpet1() {
        super(MainMod.aspectHome,MainMod.aspectVoice,null);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityWelcomeCarpet1();
	}
	
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	if (!par1World.isRemote)
    	{
    		TileEntityWelcomeCarpet1 te = (TileEntityWelcomeCarpet1) par1World.getTileEntity(par2, par3, par4);
    		
    		if (par5EntityPlayer.isSneaking())
    		{
    			par1World.setBlock(par2, par3, par4, MainMod.welcomeMatSouth);
    			return true;
    		}
    		else
    		{
    			par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "Welcome " + par5EntityPlayer.getCommandSenderName() + "!"));
    		}
    	}
    	return false;
    }
    
    
    public void onEntityWalking(World par1World, int par2, int par3, int par4, Entity par5Entity)
    {
    	if (par1World.isRemote)
    	{
    		TileEntityWelcomeCarpet1 te = (TileEntityWelcomeCarpet1) par1World.getTileEntity(par2, par3, par4);
    		
    		if (par5Entity instanceof EntityPlayer)
    		{
    				((EntityPlayer)par5Entity).addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "Welcome " + ((EntityPlayer)par5Entity).getCommandSenderName() + "!"));
    		}
    	}
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
    public void registerIcons(IIconRegister icon) {
            this.blockIcon = icon.registerIcon(MainMod.MOD_ID + ":" + "wIcon");
    }

    public int idDropped(int par1, Random par2Random, int par3)
    {
    	return blockRegistry.getIDForObject(MainMod.welcomeMat);    	
    }
	

}
