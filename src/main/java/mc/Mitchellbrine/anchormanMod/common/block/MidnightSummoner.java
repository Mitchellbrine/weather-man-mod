package mc.Mitchellbrine.anchormanMod.common.block;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityMidnightCarpet;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;

public class MidnightSummoner extends WeatherCarpet {

	public MidnightSummoner() {
        super(MainMod.aspectNight, MainMod.aspectTime,null);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityMidnightCarpet();
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	
    	if (!par1World.isRemote)
    	{
        WorldServer worldserver = MinecraftServer.getServer().worldServers[0];
        WorldInfo worldinfo = worldserver.getWorldInfo();
        
        if (par5EntityPlayer.isSneaking())
        { 	
		worldinfo.setWorldTime(18000);
		par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + "Time has been set to: MIDNIGHT"));
		CarpetUtilities.broadcastMessage(par1World, par5EntityPlayer.getCommandSenderName() + " has changed the time to MIDNIGHT!");
		return true;
        }
    	}
    	
    	return false;
    }
    
    
    //This is the icon to use for showing the block in your hand.
//    public void registerIcons(IIconRegister icon) {
//            this.blockIcon = icon.registerIcon(MainMod.MOD_ID + ":" + "mSIcon");
//    }
	

}
