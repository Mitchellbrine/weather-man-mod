package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.List;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class CarpetUtilities {

	public static void broadcastMessage(World par1World, String par2String) {
		
		List<EntityPlayerMP> players = par1World.playerEntities;
		
		for (int t = 0; t < players.size(); t++) {
			if (MainMod.notifyAll == false) {
				if (players.get(t).mcServer.getCommandManager().getPossibleCommands(players.get(t)).size() > 3) {
					players.get(t).addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "[Weather Carpets: " + par2String + "]"));
				}
			}
			else {
                if (players.get(t).mcServer.getCommandManager().getPossibleCommands(players.get(t)).size() > 3) {
					players.get(t).addChatMessage(new ChatComponentText("["+ EnumChatFormatting.AQUA + "Weather Carpets" + EnumChatFormatting.WHITE + "] " + EnumChatFormatting.AQUA + par2String));
				}				
			}
		}
		
	}
	
}
