package mc.Mitchellbrine.anchormanMod.common.block;

import java.util.Random;

import mc.Mitchellbrine.anchormanMod.aspects.AspectHelper;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityCarpetFormerFilled;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.command.server.CommandWhitelist;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

public class CarpetFormerFilled extends BlockContainer {
		
    private ItemStack needleWithThread = new ItemStack(MainMod.needleWithThread);
    private ItemStack needleWithLinearThread = new ItemStack(MainMod.needleWithLinearThread);
	
	public CarpetFormerFilled() {
		super(Material.cloth);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0625F, 1.0F);
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }

	@Override
	public TileEntity createNewTileEntity(World var1, int var2) {
		return new TileEntityCarpetFormerFilled();
	}
	
	
	
	public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9) {
			ItemStack currentItem = par5EntityPlayer.inventory.getCurrentItem();
		
			if (!par5EntityPlayer.isSneaking()) {
				if (par5EntityPlayer.getCurrentEquippedItem() != null) {
					if (currentItem.getItem() == Items.potionitem && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectPluviae",3,"aspectCaelum",2)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Items.potionitem);
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.rainSummoner), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(Blocks.iron_block) && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectPluviae",6,"aspectCaelum",5)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(Blocks.iron_block));
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.thunderSummoner), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);						
					}
					else if (currentItem.getItem() == Items.bed && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectAmuletum",6,"aspectDomum",6)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Items.bed);
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.spawnSummoner), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);							
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(Blocks.tnt) && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectCaelum",3,"aspectInpero",6)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(Blocks.tnt));
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.launcherCarpet), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(Blocks.bookshelf) && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectAmuletum",10,"aspectForma",4)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(Blocks.bookshelf));
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.expCarpet), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(MainMod.bonemealBlock) && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectDomum",7,"aspectForma",5)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(MainMod.bonemealBlock));
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.farmCarpet), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
					}
					else if (currentItem.getItem() == Items.blaze_powder && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectAmuletum",5,"aspectInfernus",10)) {
						par5EntityPlayer.inventory.consumeInventoryItem(Items.blaze_powder);
						par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.netherCarpet), 1));
						par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
					}
                    else if (currentItem.getItem() == Items.diamond_pickaxe && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectTerra",7,"aspectForma",4)) {
                        par5EntityPlayer.inventory.consumeInventoryItem(Items.diamond_pickaxe);
                        par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.miningCarpet), 1));
                        par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
                    }
                    else if (currentItem.getItem() == Item.getItemFromBlock(Blocks.tripwire_hook) && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectMortem",6,"aspectInpero",6)) {
                        par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(Blocks.tripwire_hook));
                        par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.trappedCarpet), 1));
                        par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
                    }
                    else if (currentItem.getItem() == Item.getItemFromBlock(Blocks.wooden_pressure_plate) && inventoryContainsNeedle(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectInpero",7)) {
                        par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(Blocks.wooden_pressure_plate));
                        par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.detectionCarpet), 1));
                        par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
                    }
					else if (currentItem.getItem() == Item.getItemFromBlock(MainMod.noonWool) && inventoryContainsNeedleLinear(par5EntityPlayer)) {
						if (this.hasLinearMastery(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectLux",6,"aspectAetas",6)) {
							par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(MainMod.noonWool));
							par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.noonSummoner), 1));
							par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
						}
						else if (!this.hasLinearMastery(par5EntityPlayer)){
						if (!par1World.isRemote){
						par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "This process is too complicated for you..."));
						}
						return false;
						}
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(MainMod.midnightWool) && inventoryContainsNeedleLinear(par5EntityPlayer)) {
						if (this.hasLinearMastery(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectNox",6,"aspectAetas",6)) {
							par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(MainMod.midnightWool));
							par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.midnightSummoner), 1));
							par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
							needleWithLinearThread.damageItem(1, par5EntityPlayer);
						}
						else if (!this.hasLinearMastery(par5EntityPlayer)){
						if (!par1World.isRemote){
						par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "This process is too complicated for you..."));
						}
						return false;
						}	
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(MainMod.dawnWool) && inventoryContainsNeedleLinear(par5EntityPlayer)) {
						if (this.hasLinearMastery(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectLux",6,"aspectNox",4,"aspectAetas",6)) {
							par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(MainMod.dawnWool));
							par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.dawnSummoner), 1));
							par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
							needleWithLinearThread.damageItem(1, par5EntityPlayer);
						}
						else if (!this.hasLinearMastery(par5EntityPlayer)){
							if (!par1World.isRemote){	
						par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "This process is too complicated for you..."));
							}
						return false;
						}	
					}
					else if (currentItem.getItem() == Item.getItemFromBlock(MainMod.duskWool) && inventoryContainsNeedleLinear(par5EntityPlayer)) {
						if (this.hasLinearMastery(par5EntityPlayer) && AspectHelper.areAspectsMet(par5EntityPlayer,"aspectLux",4,"aspectNox",6,"aspectAetas",6)) {
							par5EntityPlayer.inventory.consumeInventoryItem(Item.getItemFromBlock(MainMod.duskWool));
							par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(Item.getItemFromBlock(MainMod.duskSummoner), 1));
							par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
							needleWithLinearThread.damageItem(1, par5EntityPlayer);
						}
						else if (!this.hasLinearMastery(par5EntityPlayer)){
							if (!par1World.isRemote){	
						par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "This process is too complicated for you..."));
							}
						return false;
						}
					}
					else {
						if (!par5EntityPlayer.inventory.hasItem(MainMod.needleWithThread) & !par5EntityPlayer.inventory.hasItem(MainMod.needleWithLinearThread)) {
						par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "Needles required!"));
                        }
						return false;
					}
				}
                else {
                    par5EntityPlayer.inventory.addItemStackToInventory(new ItemStack(MainMod.magicalCarpetTemplate, 1));
                    par1World.setBlock(par2, par3, par4, MainMod.carpetFormer);
                }
			}
				return true;
	}
	
	private boolean inventoryContainsNeedle(EntityPlayer par5EntityPlayer) {
		if (par5EntityPlayer.inventory.hasItem(MainMod.needleWithThread))
		{
			return true;
		}
		return false;
	}
	
	private boolean inventoryContainsNeedleLinear(EntityPlayer par5EntityPlayer) {
		if (par5EntityPlayer.inventory.hasItem(MainMod.needleWithLinearThread))
		{
			return true;
		}
		return false;
	}

	public boolean isFlammable(IBlockAccess par1BlockAccess, int x, int y, int z, ForgeDirection direction) {
		return true;
	}
	
    public int idDropped(int par1, Random par2Random, int par3)
    {
		return blockRegistry.getIDForObject(MainMod.carpetFormer);
    	
    }

    private boolean hasLinearMastery(EntityPlayer player) {
        if (!player.worldObj.isRemote) {
            EntityPlayerMP playerMP = (EntityPlayerMP) player;
            if (playerMP.func_147099_x().hasAchievementUnlocked(MainMod.linearMastery)) {
                return true;
            }
            return false;
        } else {
            EntityClientPlayerMP playerMP = (EntityClientPlayerMP) player;
            if (playerMP.getStatFileWriter().hasAchievementUnlocked(MainMod.linearMastery)) {
                return true;
            }
            return false;
        }
    }
   
}
