package mc.Mitchellbrine.anchormanMod.common.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityDetectionCarpet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockPressurePlate;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class DetectionCarpet extends BlockPressurePlate{

    private boolean powered = false;

    private BlockPressurePlate.Sensitivity pressureType;
    private String iconName;
    private int type;

    public DetectionCarpet(int type) {
        super(null, Material.cloth,null);
        if (type == 0) {
            this.pressureType = Sensitivity.everything;
            this.iconName = "planks_oak";
            this.type = type;
        } else if (type == 1) {
            this.pressureType = Sensitivity.players;
            this.iconName = "stone";
            this.type = type;
        } else if (type == 2) {
            this.pressureType = Sensitivity.mobs;
            this.iconName = "iron_block";
            this.type = type;
        }
        this.setTickRandomly(true);
    }

    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int meta, float hitX, float hitY, float hitZ) {
        if (this.type == 0) {
            world.setBlock(x,y,z, MainMod.detectionCarpet1);
            if (world.isRemote) player.addChatMessage(new ChatComponentTranslation("carpet.detection.change1").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA).setItalic(true)));
        } else if (this.type == 1) {
            world.setBlock(x, y, z, MainMod.detectionCarpet2);
            if (world.isRemote) player.addChatMessage(new ChatComponentTranslation("carpet.detection.change2").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA).setItalic(true)));
        } else if (this.type == 2) {
            world.setBlock(x,y,z,MainMod.detectionCarpet);
            if (world.isRemote) player.addChatMessage(new ChatComponentTranslation("carpet.detection.change0").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA).setItalic(true)));
        }
        return true;
    }

    protected int func_150066_d(int p_150066_1_)
    {
        return p_150066_1_ > 0 ? 1 : 0;
    }

    protected int func_150060_c(int p_150060_1_)
    {
        return p_150060_1_ == 1 ? 15 : 0;
    }

    protected int func_150065_e(World p_150065_1_, int p_150065_2_, int p_150065_3_, int p_150065_4_)
    {
        List list = null;

        if (this.pressureType == BlockPressurePlate.Sensitivity.everything)
        {
            list = p_150065_1_.getEntitiesWithinAABBExcludingEntity((Entity)null, this.func_150061_a(p_150065_2_, p_150065_3_, p_150065_4_));
        }

        if (this.pressureType == BlockPressurePlate.Sensitivity.mobs)
        {
            list = p_150065_1_.getEntitiesWithinAABB(EntityLivingBase.class, this.func_150061_a(p_150065_2_, p_150065_3_, p_150065_4_));
        }

        if (this.pressureType == BlockPressurePlate.Sensitivity.players)
        {
            list = p_150065_1_.getEntitiesWithinAABB(EntityPlayer.class, this.func_150061_a(p_150065_2_, p_150065_3_, p_150065_4_));
        }

        if (list != null && !list.isEmpty())
        {
            Iterator iterator = list.iterator();

            while (iterator.hasNext())
            {
                Entity entity = (Entity)iterator.next();

                if (!entity.doesEntityNotTriggerPressurePlate())
                {
                    return 15;
                }
            }
        }

        return 0;
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister p_149651_1_)
    {
        this.blockIcon = p_149651_1_.registerIcon(this.iconName);
    }

    public ItemStack getPickBlock(MovingObjectPosition mop,World world, int x, int y, int z) {
        return new ItemStack(MainMod.detectionCarpet,1);
    }

    public Item getItemDropped(int par1, Random random, int par3) {
        return Item.getItemFromBlock(MainMod.detectionCarpet);
    }
    
}
