package mc.Mitchellbrine.anchormanMod.common.block;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityOmniCarpet;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class OmniCarpet extends WeatherCarpet{

    public OmniCarpet() {
        super(MainMod.aspectCharge,null,null);
    }

    @Override
    public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
        return new TileEntityOmniCarpet();
    }

}
