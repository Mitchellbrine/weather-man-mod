package mc.Mitchellbrine.anchormanMod.common.block;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityExpCarpet;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

public class ExperienceCarpet extends WeatherCarpet {



	public ExperienceCarpet() {
		super(MainMod.aspectCharm,MainMod.aspectForma,null);
		this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.0625F, 1.0F);
	}

	@Override
	public TileEntity createNewTileEntity(World world, int par2) {
		return new TileEntityExpCarpet();
	}
	
    //You don't want the normal render type, or it wont render properly.
    @Override
    public int getRenderType() {
            return -1;
    }
    
    //It's not an opaque cube, so you need this.
    @Override
    public boolean isOpaqueCube() {
            return false;
    }
    
    //It's not a normal block, so you need this too.
    public boolean renderAsNormalBlock() {
            return false;
    }
    
    public boolean onBlockActivated(World par1World, int par2, int par3, int par4, EntityPlayer par5EntityPlayer, int par6, float par7, float par8, float par9)
    {
    	
    	if (!par1World.isRemote)
    	{
        if (par5EntityPlayer.isSneaking())
        {
        	if (par5EntityPlayer.inventory.hasItem(Items.experience_bottle) || par5EntityPlayer.capabilities.isCreativeMode)
        	{
        		if (!par5EntityPlayer.capabilities.isCreativeMode) {
        		par5EntityPlayer.inventory.consumeInventoryItem(Items.experience_bottle);
        		}
        		par5EntityPlayer.addExperienceLevel(1);
        		par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Experience granted!"));
        	}
        	else
        	{
        		par5EntityPlayer.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Bottles needed!"));        		
        	}
		return true;
        }
        }
    	else if (par1World.isRemote)
    	{
        if (par5EntityPlayer.isSneaking())
        {
        	if (par5EntityPlayer.inventory.hasItem(Items.experience_bottle) || par5EntityPlayer.capabilities.isCreativeMode)
        	{
        		if (!par5EntityPlayer.capabilities.isCreativeMode) {
        		par5EntityPlayer.inventory.consumeInventoryItem(Items.experience_bottle);
        		}
        		par5EntityPlayer.addExperienceLevel(1);
        	}
        	else
        	{
        	}
		return true;
        }
        }
    	
    	return false;
    }
    
    
    
    //This is the icon to use for showing the block in your hand.
//    public void registerIcons(IIconRegister icon) {
//            this.blockIcon = icon.registerIcon(MainMod.MOD_ID + ":" + "expIcon");
//    }

	

}
