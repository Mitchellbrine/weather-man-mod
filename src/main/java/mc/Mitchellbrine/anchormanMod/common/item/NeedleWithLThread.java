package mc.Mitchellbrine.anchormanMod.common.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.item.Item;

public class NeedleWithLThread extends Item {

	public NeedleWithLThread() {
	this.setMaxDamage(10);
	this.setMaxStackSize(1);
	}
	
    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    @SideOnly(Side.CLIENT)
    public boolean isFull3D()
    {
        return true;
    }


}
