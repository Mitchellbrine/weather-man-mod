package mc.Mitchellbrine.anchormanMod.common.effects;

import mc.Mitchellbrine.anchormanMod.common.block.CarpetUtilities;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraft.world.storage.WorldInfo;

import java.util.Random;

public class CarpetEffects {

    public static void timeChange(World world, EntityPlayer player,String time) {
        if (!player.worldObj.isRemote) {
            WorldServer worldserver = MinecraftServer.getServer().worldServers[0];
            WorldInfo worldinfo = worldserver.getWorldInfo();

            if (player.isSneaking()) {

                int timeS = 0;

                if (time.toLowerCase().equals("dawn")){ timeS = 0; }
                if (time.toLowerCase().equals("noon")){ timeS = 1; }
                if (time.toLowerCase().equals("dusk")){ timeS = 2; }
                if (time.toLowerCase().equals("midnight")){ timeS = 3; }

                switch (timeS) {
                    case 0: worldinfo.setWorldTime(0);
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + "Time has been set to: DAWN"));
                        CarpetUtilities.broadcastMessage(world, player.getCommandSenderName() + " has changed the time to DAWN!");
                        break;
                    case 1: worldinfo.setWorldTime(6000);
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + "Time has been set to: NOON"));
                        CarpetUtilities.broadcastMessage(world, player.getCommandSenderName() + " has changed the time to NOON!");
                        break;
                    case 2: worldinfo.setWorldTime(12000);
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + "Time has been set to: DUSK"));
                        CarpetUtilities.broadcastMessage(world, player.getCommandSenderName() + " has changed the time to DUSK!");
                        break;
                    case 3: worldinfo.setWorldTime(18000);
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + "Time has been set to: MIDNIGHT"));
                        CarpetUtilities.broadcastMessage(world, player.getCommandSenderName() + " has changed the time to MIDNIGHT!");
                        break;
                    default: break;
                }
            }
        }
    }

    public static void rainEffect(World world, EntityPlayer player) {

        if (!world.isRemote)
        {
            WorldServer worldserver = MinecraftServer.getServer().worldServers[0];
            WorldInfo worldinfo = worldserver.getWorldInfo();

            if (player.isSneaking() && worldinfo.getRainTime() > 0)
            {
                worldinfo.setRaining(true);
                player.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Rain has been summoned!"));
                CarpetUtilities.broadcastMessage(world, player.getCommandSenderName() + " has summoned rain!");

            }
            else
            {
                worldinfo.setRaining(false);
                player.addChatMessage(new ChatComponentText(EnumChatFormatting.AQUA + "Rain has been banished!"));
                CarpetUtilities.broadcastMessage(world, player.getCommandSenderName() + " has banished the rain!");

            }
        }

    }

    public static void thunderEffect(World world, EntityPlayer player, int x, int y, int z) {
        if (!world.isRemote)
        {
            WorldServer worldserver = MinecraftServer.getServer().worldServers[0];
            WorldInfo worldinfo = worldserver.getWorldInfo();

            if (player.isSneaking())
            {
                world.addWeatherEffect(new EntityLightningBolt(world, x, y, z));
                player.addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "Thunder has been summoned!"));
            }
        }
    }

    public static void setSpawn(World world, EntityPlayer player) {
        if (!world.isRemote)
        {
            EntityPlayerMP entityplayermp = (EntityPlayerMP) player;

            if (player.isSneaking())
            {
                ChunkCoordinates chunkcoordinates = entityplayermp.getPlayerCoordinates();
                player.setSpawnChunk(chunkcoordinates, true);
                player.addChatMessage(new ChatComponentText(EnumChatFormatting.LIGHT_PURPLE + "Spawn has been set"));
            }
        }
    }

    public static void addExperience(World world, EntityPlayer player) {
            if (player.isSneaking())
            {
                if (player.inventory.hasItem(Items.experience_bottle) || player.capabilities.isCreativeMode)
                {
                    if (!player.capabilities.isCreativeMode) {
                        player.inventory.consumeInventoryItem(Items.experience_bottle);
                    }
                    player.addExperienceLevel(1);
                    if (!world.isRemote) {
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Experience granted!"));
                    }
                }
                else
                {
                    if (!world.isRemote) {
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Bottles needed!"));
                    }
                }
            }
    }

    public static void farmItems(World world, EntityPlayer player, int x1, int y1, int z1) {
        if (!world.isRemote)
        {
            if (!player.isSneaking())
            {
                if ((player.inventory.hasItem(Items.wheat_seeds) || player.inventory.hasItem(Items.pumpkin_seeds) || player.inventory.hasItem(Items.melon_seeds) || player.inventory.hasItem(Items.carrot) || player.inventory.hasItem(Items.potato)))
                {
                    if (player.inventory.hasItem(Items.wheat_seeds) || player.getCurrentEquippedItem().getItem() == Items.wheat_seeds)
                    {
                        EntityPlayerMP entityplayermp = (EntityPlayerMP)player;
                        if (player.getCurrentEquippedItem().getItem() == Items.wheat_seeds) {
                            if (!entityplayermp.func_147099_x().hasAchievementUnlocked(MainMod.noLongerOfftopic)) {
                                player.addStat(MainMod.noLongerOfftopic, 1);
                            }
                        }
                        player.inventory.consumeInventoryItem(Items.wheat_seeds);
                        player.inventory.addItemStackToInventory(new ItemStack(Items.wheat, 1));
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Wheat grown!"));

                    }
                    else if (player.inventory.hasItem(Items.pumpkin_seeds) || player.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds)
                    {
                        player.inventory.consumeInventoryItem(Items.pumpkin_seeds);
                        player.inventory.addItemStackToInventory(new ItemStack(Blocks.pumpkin, 1));
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Pumpkin grown!"));
                    }
                    else if (player.inventory.hasItem(Items.melon_seeds) || player.getCurrentEquippedItem().getItem() == Items.melon_seeds)
                    {
                        player.inventory.consumeInventoryItem(Items.melon_seeds);
                        player.inventory.addItemStackToInventory(new ItemStack(Blocks.melon_block, 1));
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Melon grown!"));
                    }

                    if (player.inventory.hasItem(Items.carrot) || player.getCurrentEquippedItem().getItem() == Items.carrot)
                    {
                        player.inventory.addItemStackToInventory(new ItemStack(Items.carrot, 1));
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Carrot grown!"));
                    }

                    if (player.inventory.hasItem(Items.potato) || player.getCurrentEquippedItem().getItem() == Items.potato)
                    {
                        player.inventory.addItemStackToInventory(new ItemStack(Items.potato, 1));
                        player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Potato grown!"));
                    }
                }
                else
                {
                    if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem().getUnlocalizedName().contains("hoe")) {
                        if (player.getCurrentEquippedItem().getItemDamage() <= player.getCurrentEquippedItem().getMaxDamage() - 4) {
                            for (int x = x1-1; x < x1 + 2; x++) {
                                for (int z = z1-1; z < z1 + 2; z++) {
                                    System.err.println(x + " " + (y1 - 1) + " " + z);
                                    if (world.getBlock(x,y1-1,z) == Blocks.dirt || world.getBlock(x,y1-1,z) == Blocks.grass) {
                                        world.setBlock(x, y1 - 1, z, Blocks.farmland);
                                    }
                                }
                            }
                        }
                    }
                    player.addChatMessage(new ChatComponentText(EnumChatFormatting.YELLOW + "Seeds required for farming!"));
                }
            }
            else {

            }
        }
        else
        {
            if (!player.isSneaking())
            {
                if ((player.inventory.hasItem(Items.wheat_seeds) || player.inventory.hasItem(Items.pumpkin_seeds) || player.inventory.hasItem(Items.melon_seeds) || player.inventory.hasItem(Items.carrot) || player.inventory.hasItem(Items.potato)))
                {

                    if (player.inventory.hasItem(Items.wheat_seeds) || player.getCurrentEquippedItem().getItem() == Items.wheat_seeds)
                    {
                        player.inventory.consumeInventoryItem(Items.wheat_seeds);
                        player.inventory.addItemStackToInventory(new ItemStack(Items.wheat, 1));
                    }
                    else if (player.inventory.hasItem(Items.pumpkin_seeds) || player.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds)
                    {
                        player.inventory.consumeInventoryItem(Items.pumpkin_seeds);
                        player.inventory.addItemStackToInventory(new ItemStack(Blocks.pumpkin, 1));
                    }
                    else if (player.inventory.hasItem(Items.melon_seeds) || player.getCurrentEquippedItem().getItem() == Items.melon_seeds)
                    {
                        player.inventory.consumeInventoryItem(Items.melon_seeds);
                        player.inventory.addItemStackToInventory(new ItemStack(Blocks.melon_block, 1));
                    }

                    if (player.inventory.hasItem(Items.carrot) || player.getCurrentEquippedItem().getItem() == Items.carrot)
                    {
                        player.inventory.addItemStackToInventory(new ItemStack(Items.carrot, 1));
                    }

                    if (player.inventory.hasItem(Items.potato) || player.getCurrentEquippedItem().getItem() == Items.potato)
                    {
                        player.inventory.addItemStackToInventory(new ItemStack(Items.potato, 1));
                    }
                }
            }
        }
    }

    public static void netherTransportation(World world, EntityPlayer player, int x, int y, int z) {
        if (!world.isRemote)
        {
            if (player.isSneaking()) {
                if (player.ridingEntity == null && player.riddenByEntity == null)
                {
                    player.setInPortal();
                    switch (player.dimension) {
                        case 0: player.travelToDimension(-1); break;
                        case -1: player.travelToDimension(0); break;
                        default: break;
                    }
                }
            }
        }
    }

    public static void mine3x3(World world, EntityPlayer player, int x, int y, int z) {
        if (!world.isRemote)
        {
            if (!player.isSneaking()) {
                if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem().getUnlocalizedName().contains("pickaxe") && player.getCurrentEquippedItem().getItem().getHarvestLevel(player.getCurrentEquippedItem(),"pickaxe") > 1) {
                    if (player.getCurrentEquippedItem().getItemDamage() <= player.getCurrentEquippedItem().getMaxDamage() - 13) {

                        int damageFromUse = 0;
                        Random random = new Random();

                        /*//Bottom Row

                        if (world.getBlock(x-1,y-1,z-1).getBlockHardness(world,x-1,y-1,z-1) > && world.func_147480_a(x-1,y-1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x-1,y-1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x-1,y-1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y-1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y-1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y-1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y-1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y-1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y-1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}

                        // Middle Row

                        if (world.func_147480_a(x-1,y,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x-1,y,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x-1,y,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}

                        // Top Row

                        if (world.func_147480_a(x-1,y+1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x-1,y+1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x-1,y+1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y+1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y+1,z+1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y+1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y+1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x+1,y+1,z,true)) { if (random.nextInt(100) > 50) damageFromUse++;}
                        if (world.func_147480_a(x,y+1,z-1,true)) { if (random.nextInt(100) > 50) damageFromUse++;}

                        player.getCurrentEquippedItem().damageItem(damageFromUse,player); */

                        for (int x1 = x - 1; x1 < x + 2;x1++) {

                            for (int y1 = y - 1; y1 < y + 2;y1++) {

                                for (int z1 = z - 1; z1 < z + 2;z1++) {

                                    if ((world.getBlock(x1,y1,z1).getBlockHardness(world,x1,y1,z1) < 50 || world.getBlock(x1,y1,z1) != Blocks.bedrock) && world.getBlock(x1,y1,z1) != world.getBlock(x,y,z)) {
                                        world.func_147480_a(x1,y1,z1,true);
                                        if (random.nextInt(100) > 50) {
                                            player.getCurrentEquippedItem().damageItem(1,player);
                                        }
                                    }

                                }

                            }

                        }

                    }
                }
                else if (player.getCurrentEquippedItem() == null) {
                    player.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "" + EnumChatFormatting.ITALIC + "Pickaxe needed!"));
                } else {
                    player.addChatMessage(new ChatComponentText(EnumChatFormatting.GOLD + "" + EnumChatFormatting.ITALIC + "Pickaxe needed!"));
                }
            }
        }
    }

    public static void unleashTrap(World world, EntityPlayer player, int x, int y, int z) {
        world.createExplosion(player,x,y+1,z,7.0F,false);
        player.motionY += 1;
        for (int x1 = x - 1; x1 < x + 2; x1++) {
            for (int y1 = y - 20; y1 < y + 1; y1++) {
                for (int z1 = z - 1; z1 < z + 2;z1++) {
                    if ((world.getBlock(x1,y1,z1).getBlockHardness(world,x1,y1,z1) < 50 || world.getBlock(x1,y1,z1) != Blocks.bedrock)) {
                        world.func_147480_a(x1, y1, z1, true);
                    }
                }
            }
        }
    }

    // Required for Farm Carpet

    public static boolean isRequiredSeeds(EntityPlayer player) {
        if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() == Items.wheat_seeds || player.getCurrentEquippedItem().getItem() == Items.melon_seeds || player.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds || player.getCurrentEquippedItem().getItem() == Items.carrot || player.getCurrentEquippedItem().getItem() == Items.potato) {
            return true;
        }
        else if (player.getCurrentEquippedItem() == null && player.inventory.hasItem(Items.wheat_seeds) || player.inventory.hasItem(Items.melon_seeds) || player.inventory.hasItem(Items.pumpkin_seeds) || player.inventory.hasItem(Items.carrot) || player.inventory.hasItem(Items.potato)) {
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean isRequiredSeedsClient(EntityClientPlayerMP player) {
        if (player.getCurrentEquippedItem() != null && player.getCurrentEquippedItem().getItem() == Items.wheat_seeds || player.getCurrentEquippedItem().getItem() == Items.melon_seeds || player.getCurrentEquippedItem().getItem() == Items.pumpkin_seeds || player.getCurrentEquippedItem().getItem() == Items.carrot || player.getCurrentEquippedItem().getItem() == Items.potato) {
            return true;
        }
        else if (player.getCurrentEquippedItem() == null && player.inventory.hasItem(Items.wheat_seeds) || player.inventory.hasItem(Items.melon_seeds) || player.inventory.hasItem(Items.pumpkin_seeds) || player.inventory.hasItem(Items.carrot) || player.inventory.hasItem(Items.potato)) {
            return true;
        }
        else {
            return false;
        }
    }
    
}