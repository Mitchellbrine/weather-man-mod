package mc.Mitchellbrine.anchormanMod.aspects;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;

public class AspectEvents {

    @SubscribeEvent
    public void takeAspects(PlayerEvent.ItemPickupEvent event) {
        if (event.pickedUp.getEntityItem().getItem() instanceof Aspect) {
            if (event.player.getEntityData().hasKey(event.pickedUp.getEntityItem().getUnlocalizedName().substring(5))) {
                for (int i = 0;i<event.player.inventory.getSizeInventory();i++) {
                    if (event.player.inventory.getStackInSlot(i) != null && event.player.inventory.getStackInSlot(i).getItem() == event.pickedUp.getEntityItem().getItem()) {
                        if (event.player.inventory.getStackInSlot(i).stackSize > 0) {
                            event.player.inventory.consumeInventoryItem(event.player.inventory.getStackInSlot(i).getItem());
                            int value = event.player.getEntityData().getInteger(event.pickedUp.getEntityItem().getUnlocalizedName().substring(5)) + 1;
                            event.player.getEntityData().setInteger(event.pickedUp.getEntityItem().getUnlocalizedName().substring(5), value);
                            if ((value % 10 == 0)) {
                                event.player.worldObj.playSoundAtEntity(event.player,"anchorman:aspectLevelUp",1.0F,1.0F);
                                event.player.addChatComponentMessage(new ChatComponentTranslation("aspect.level.ten").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.DARK_RED)));
                                event.player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.DARK_RED + "" + value + " " + event.pickedUp.getEntityItem().getUnlocalizedName().substring(11)));
                            } else if ((value % 5) == 0){
                                event.player.worldObj.playSoundAtEntity(event.player,"random.levelup",1.0F,1.0F);
                                event.player.addChatComponentMessage(new ChatComponentTranslation("aspect.level.five").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
                                event.player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.RED + "" + value + " " + event.pickedUp.getEntityItem().getUnlocalizedName().substring(11)));
                            } else {
                                event.player.worldObj.playSoundAtEntity(event.player,"random.orb",1.0F,1.0F);
                                event.player.addChatComponentMessage(new ChatComponentTranslation("aspect.level.new").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD)));
                                event.player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GOLD + "" + value + " " + event.pickedUp.getEntityItem().getUnlocalizedName().substring(11)));
                            }
                        }
                    }
                }
            } else {
                for (int i = 0;i<event.player.inventory.getSizeInventory();i++) {
                    if (event.player.inventory.getStackInSlot(i) != null && event.player.inventory.getStackInSlot(i).getItem() == event.pickedUp.getEntityItem().getItem()) {
                        if (event.player.inventory.getStackInSlot(i).stackSize > 0) {
                            event.player.inventory.consumeInventoryItem(event.player.inventory.getStackInSlot(i).getItem());
                            event.player.getEntityData().setInteger(event.pickedUp.getEntityItem().getUnlocalizedName().substring(5), 1);
                            event.player.worldObj.playSoundAtEntity(event.player, "random.orb", 1.0F, 1.0F);
                            event.player.addChatComponentMessage(new ChatComponentTranslation("aspect.level.discover").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD)));
                            event.player.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GOLD + "" + 1 + " " + event.pickedUp.getEntityItem().getUnlocalizedName().substring(11)));
                        }
                    }
                }
            }
        }
    }

}
