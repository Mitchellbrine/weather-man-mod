package mc.Mitchellbrine.anchormanMod.aspects;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.ChatStyle;
import net.minecraft.util.EnumChatFormatting;

public class AspectHelper {

    public static boolean areAspectsMet(EntityPlayer player, String aspect, int min) {
        if (!player.worldObj.isRemote) {
            if (player.getEntityData().hasKey(aspect)) {
                if (player.getEntityData().getInteger(aspect) >= min) {
                    if (consumeAspect(player, aspect, min)) {
                        return true;
                    }
                    return false;
                } else {
                    player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                }
                return false;
            } else {
                player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
            }
        }
        return false;
    }

    public static boolean areAspectsMet(EntityPlayer player, String aspect, int min, String aspect2, int min2) {
        if (!player.worldObj.isRemote) {
            if (player.getEntityData().hasKey(aspect)) {
                if (player.getEntityData().getInteger(aspect) >= min) {
                    if (player.getEntityData().hasKey(aspect2)) {
                        if (player.getEntityData().getInteger(aspect2) >= min2) {
                            if (consumeAspect(player, aspect, min) && consumeAspect(player, aspect2, min2)) {
                                return true;
                            }
                        } else {
                            player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                        }
                    } else {
                        player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                    }
                } else {
                    player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                }
            } else {
                player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
            }
        }
        return false;
    }

    public static boolean areAspectsMet(EntityPlayer player, String aspect, int min, String aspect2, int min2, String aspect3, int min3) {
        if (!player.worldObj.isRemote) {
            if (player.getEntityData().hasKey(aspect)) {
                if (player.getEntityData().getInteger(aspect) >= min) {
                    if (player.getEntityData().hasKey(aspect2)) {
                        if (player.getEntityData().getInteger(aspect2) >= min2) {
                            if (player.getEntityData().hasKey(aspect3)) {
                                if (player.getEntityData().getInteger(aspect3) >= min3) {
                                    if (consumeAspect(player, aspect, min) && consumeAspect(player, aspect2, min2) && consumeAspect(player, aspect3, min3)) {
                                        return true;
                                    }
                                } else {
                                    player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                                }
                            } else {
                                player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                            }
                        } else {
                            player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                        }
                    } else {
                        player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                    }
                } else {
                    player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
                }
            } else {
                player.addChatComponentMessage(new ChatComponentTranslation("aspect.missing").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.AQUA)));
            }
        }
        return false;
    }

    public static boolean consumeAspect(EntityPlayer player, String aspect, int amount) {
        if (player.getEntityData().hasKey(aspect)) {
            if (player.getEntityData().getInteger(aspect) >= amount) {
               int value = player.getEntityData().getInteger(aspect) - amount;
                player.getEntityData().setInteger(aspect,value);
                return true;
            }
            return false;
        }
        return false;
    }

}
