package mc.Mitchellbrine.anchormanMod.aspects;

import java.util.List;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Aspect extends Item{
	
	private String aspectName;
	
	public Aspect(String name) {
		setTextureName("anchorman:aspects/aspect"+name);
		setUnlocalizedName("aspect"+name);
		setCreativeTab(MainMod.AspectTab);
        setMaxStackSize(1);
		aspectName = name;
	}
	
	@Override
	public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4) {
		par3List.add("Type: " + aspectName);
	}
	
}
