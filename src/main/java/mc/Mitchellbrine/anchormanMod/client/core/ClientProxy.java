package mc.Mitchellbrine.anchormanMod.client.core;

import mc.Mitchellbrine.anchormanMod.client.render.*;
import mc.Mitchellbrine.anchormanMod.client.tileentity.*;
import mc.Mitchellbrine.anchormanMod.common.core.CommonProxy;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import mc.Mitchellbrine.anchormanMod.common.tileentity.*;
import net.minecraft.item.Item;
import net.minecraftforge.client.MinecraftForgeClient;
import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraftforge.common.MinecraftForge;

public class ClientProxy extends CommonProxy {

	@Override
	public void registerTileEntities() {
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityRainCarpet.class, new TileEntityCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityThunderCarpet.class, new TileEntityTCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMidnightCarpet.class, new TileEntityMCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityNoonCarpet.class, new TileEntityNCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDawnCarpet.class, new TileEntityDaCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDuskCarpet.class, new TileEntityDuCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntitySpawnCarpet.class, new TileEntitySCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityLaunchCarpet.class, new TileEntityLCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet.class, new TileEntityWCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet1.class, new TileEntityWCarpetHandler1());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet2.class, new TileEntityWCarpetHandler2());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityWelcomeCarpet3.class, new TileEntityWCarpetHandler3());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityExpCarpet.class, new TileEntityExpCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityFarmCarpet.class, new TileEntityFCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityNetherCarpet.class, new TileEntityHCarpetHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCarpetFormer.class, new TileEntityCarpetFormerHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCarpetFormerFilled.class, new TileEntityCarpetFormerFilledHandler());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCarpetFormerFilledWool.class, new TileEntityCarpetFormerFilledWoolHandler());
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMiningCarpet.class, new NewCarpetTEHandler("anchorman:textures/blocks/miningCarpet.png"));
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityTrappedCarpet.class, new NewCarpetTEHandler("anchorman:textures/blocks/welcomeCarpetTrapped.png"));
        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityDetectionCarpet.class, new TileEntityDetectionCarpetHandler());
		
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.rainSummoner), new RSHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.thunderSummoner), new TCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.dawnSummoner), new DaCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.noonSummoner), new NCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.midnightSummoner), new MCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.duskSummoner), new DuCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.spawnSummoner), new SCarpetHandler()); 
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.welcomeMat), new WCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.farmCarpet), new FCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.launcherCarpet), new LCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.expCarpet), new ExpCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.netherCarpet), new HCarpetHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.carpetFormer), new CFormerHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.carpetFormerFilled), new CFormerFilledHandler());
		MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.carpetFormerFilledWool), new CFormerFilledWoolHandler());
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.miningCarpet), new NewCarpetHandler(new TileEntityMiningCarpet()));
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.trappedCarpet), new NewCarpetHandler(new TileEntityTrappedCarpet()));
        MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(MainMod.detectionCarpet),new NewCarpetHandler(new TileEntityDetectionCarpet()));
	}
	
}
