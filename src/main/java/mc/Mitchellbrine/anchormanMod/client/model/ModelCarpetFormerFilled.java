package mc.Mitchellbrine.anchormanMod.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelCarpetFormerFilled extends ModelBase
{
  //fields
    ModelRenderer Shape1;
    ModelRenderer Shape2;
    ModelRenderer rConnecter;
    ModelRenderer fConnecter;
    ModelRenderer bRLeg;
    ModelRenderer table;
    ModelRenderer fLLeg;
    ModelRenderer fRLeg;
    ModelRenderer bLLeg;
    ModelRenderer Shape3;
  
  public ModelCarpetFormerFilled()
  {
    textureWidth = 128;
    textureHeight = 64;
    
      Shape1 = new ModelRenderer(this, 29, 34);
      Shape1.addBox(0F, 0F, 0F, 12, 2, 2);
      Shape1.setRotationPoint(-6F, 8F, 6F);
      Shape1.setTextureSize(128, 64);
      Shape1.mirror = true;
      setRotation(Shape1, 0F, 0F, 0F);
      Shape2 = new ModelRenderer(this, 29, 39);
      Shape2.addBox(0F, 0F, 0F, 2, 2, 12);
      Shape2.setRotationPoint(6F, 8F, -6F);
      Shape2.setTextureSize(128, 64);
      Shape2.mirror = true;
      setRotation(Shape2, 0F, 0F, 0F);
      rConnecter = new ModelRenderer(this, 0, 39);
      rConnecter.addBox(0F, 0F, 0F, 2, 2, 12);
      rConnecter.setRotationPoint(-8F, 8F, -6F);
      rConnecter.setTextureSize(128, 64);
      rConnecter.mirror = true;
      setRotation(rConnecter, 0F, 0F, 0F);
      fConnecter = new ModelRenderer(this, 0, 34);
      fConnecter.addBox(0F, 0F, 0F, 12, 2, 2);
      fConnecter.setRotationPoint(-6F, 8F, -8F);
      fConnecter.setTextureSize(128, 64);
      fConnecter.mirror = true;
      setRotation(fConnecter, 0F, 0F, 0F);
      bRLeg = new ModelRenderer(this, 27, 0);
      bRLeg.addBox(0F, 0F, 0F, 2, 16, 2);
      bRLeg.setRotationPoint(6F, 8F, 6F);
      bRLeg.setTextureSize(128, 64);
      bRLeg.mirror = true;
      setRotation(bRLeg, 0F, 0F, 0F);
      table = new ModelRenderer(this, 0, 19);
      table.addBox(0F, 0F, 0F, 12, 2, 12);
      table.setRotationPoint(-6F, 10F, -6F);
      table.setTextureSize(128, 64);
      table.mirror = true;
      setRotation(table, 0F, 0F, 0F);
      fLLeg = new ModelRenderer(this, 18, 0);
      fLLeg.addBox(0F, 0F, 0F, 2, 16, 2);
      fLLeg.setRotationPoint(-8F, 8F, -8F);
      fLLeg.setTextureSize(128, 64);
      fLLeg.mirror = true;
      setRotation(fLLeg, 0F, 0F, 0F);
      fRLeg = new ModelRenderer(this, 9, 0);
      fRLeg.addBox(0F, 0F, 0F, 2, 16, 2);
      fRLeg.setRotationPoint(6F, 8F, -8F);
      fRLeg.setTextureSize(128, 64);
      fRLeg.mirror = true;
      setRotation(fRLeg, 0F, 0F, 0F);
      bLLeg = new ModelRenderer(this, 0, 0);
      bLLeg.addBox(0F, 0F, 0F, 2, 16, 2);
      bLLeg.setRotationPoint(-8F, 8F, 6F);
      bLLeg.setTextureSize(128, 64);
      bLLeg.mirror = true;
      setRotation(bLLeg, 0F, 0F, 0F);
      Shape3 = new ModelRenderer(this, 49, 19);
      Shape3.addBox(0F, 0F, 0F, 12, 2, 12);
      Shape3.setRotationPoint(-6F, 7F, -6F);
      Shape3.setTextureSize(128, 64);
      Shape3.mirror = true;
      setRotation(Shape3, 0F, 0F, 0F);
  }
  
  public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
  {
    super.render(entity, f, f1, f2, f3, f4, f5);
    setRotationAngles(f, f1, f2, f3, f4, f5);
    Shape1.render(f5);
    Shape2.render(f5);
    rConnecter.render(f5);
    fConnecter.render(f5);
    bRLeg.render(f5);
    table.render(f5);
    fLLeg.render(f5);
    fRLeg.render(f5);
    bLLeg.render(f5);
    Shape3.render(f5);
  }
  
  private void setRotation(ModelRenderer model, float x, float y, float z)
  {
    model.rotateAngleX = x;
    model.rotateAngleY = y;
    model.rotateAngleZ = z;
  }
  
  public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5)
  {
    //super.setRotationAngles(f, f1, f2, f3, f4, f5);
  }

}
