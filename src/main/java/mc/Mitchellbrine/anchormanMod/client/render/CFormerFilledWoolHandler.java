package mc.Mitchellbrine.anchormanMod.client.render;

import mc.Mitchellbrine.anchormanMod.client.model.ModelCarpetFormer;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityCarpetFormerFilledWool;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class CFormerFilledWoolHandler implements IItemRenderer{

	private ModelCarpetFormer model;
	
	public CFormerFilledWoolHandler() {
		model = new ModelCarpetFormer();
	}
	
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		TileEntityRendererDispatcher.instance.renderTileEntityAt(new TileEntityCarpetFormerFilledWool(), 0.0, 0.0, 0.0, 0.0f);
	}

}
