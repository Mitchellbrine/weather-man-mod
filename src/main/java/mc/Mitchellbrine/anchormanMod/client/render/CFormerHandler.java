package mc.Mitchellbrine.anchormanMod.client.render;

import mc.Mitchellbrine.anchormanMod.client.model.ModelCarpetFormer;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityCarpetFormer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class CFormerHandler implements IItemRenderer{

	private ModelCarpetFormer model;

	public CFormerHandler() {
		model = new ModelCarpetFormer();
	}
	
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		TileEntityRendererDispatcher.instance.renderTileEntityAt(new TileEntityCarpetFormer(), 0.0, 0.0, 0.0, 180.0f);
	}

}
