package mc.Mitchellbrine.anchormanMod.client.render;

import mc.Mitchellbrine.anchormanMod.client.model.ModelCarpetFormer;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityCarpetFormerFilled;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.IItemRenderer;

public class CFormerFilledHandler implements IItemRenderer{

	private ModelCarpetFormer model;
	
	public CFormerFilledHandler() {
		model = new ModelCarpetFormer();
	}
	
	public boolean handleRenderType(ItemStack item, ItemRenderType type) {
		return true;
	}

	public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item,
			ItemRendererHelper helper) {
		return true;
	}

	@Override
	public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
		TileEntityRendererDispatcher.instance.renderTileEntityAt(new TileEntityCarpetFormerFilled(), 0.0, 0.0, 0.0, 0.0f);
	}

}
