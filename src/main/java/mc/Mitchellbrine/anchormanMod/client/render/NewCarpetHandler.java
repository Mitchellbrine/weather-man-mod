package mc.Mitchellbrine.anchormanMod.client.render;

import mc.Mitchellbrine.anchormanMod.client.model.ModelRainSummoner;
import mc.Mitchellbrine.anchormanMod.common.tileentity.TileEntityNoonCarpet;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.client.IItemRenderer;

public class NewCarpetHandler implements IItemRenderer {

    private TileEntity te;

    public NewCarpetHandler(TileEntity tileEntity) { te = tileEntity; }

    @Override
    public boolean handleRenderType(ItemStack item, ItemRenderType type) {
        return true;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemRenderType type, ItemStack item, ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemRenderType type, ItemStack item, Object... data) {
        TileEntityRendererDispatcher.instance.renderTileEntityAt(te, 0.0, 0.0, 0.0, 0.0f);

    }
}
