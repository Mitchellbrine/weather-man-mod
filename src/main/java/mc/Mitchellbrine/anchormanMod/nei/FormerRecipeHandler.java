package mc.Mitchellbrine.anchormanMod.nei;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.lang.reflect.Field;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StatCollector;

import org.lwjgl.input.Mouse;

import codechicken.nei.NEIServerUtils;
import codechicken.nei.PositionedStack;
import codechicken.nei.recipe.GuiRecipe;
import codechicken.nei.recipe.TemplateRecipeHandler;

public class FormerRecipeHandler extends TemplateRecipeHandler {
    public class CachedFormerRecipe extends CachedRecipe {
        PositionedStack input;
        PositionedStack output;
        int aspect1, aspect2, aspect3;
        String firstAspect, secondAspect, thirdAspect;
        boolean temporalThread;
        boolean isMat;

        public CachedFormerRecipe(FormerRecipe recipe) {
            input = new PositionedStack(recipe.requiredItem, 38, 2, false);
            output = new PositionedStack(recipe.result, 132, 32, false);
            aspect1 = recipe.aspect1;
            aspect2 = recipe.aspect2;
            aspect3 = recipe.aspect3;
            firstAspect = recipe.aspectString1;
            secondAspect = recipe.aspectString2;
            thirdAspect = recipe.aspectString3;
            temporalThread = recipe.temporalThread;
            isMat = recipe.isMat;
        }

        @Override
        public PositionedStack getIngredient() {
            return input;
        }

        @Override
        public PositionedStack getResult() {
            return output;
        }
    }

    @Override
    public void loadCraftingRecipes(String outputId, Object... results) {
        if (outputId.equals("formerrecipes") && getClass() == FormerRecipeHandler.class) {
            for(FormerRecipe recipe: FormerRecipeRegistry.altarRecipes) {
                if(recipe.result != null) arecipes.add(new CachedFormerRecipe(recipe));
            }
        } else {
            super.loadCraftingRecipes(outputId, results);
        }
    }

    @Override
    public void loadCraftingRecipes(ItemStack result) {
        for(FormerRecipe recipe: FormerRecipeRegistry.altarRecipes) {
            if(NEIServerUtils.areStacksSameTypeCrafting(recipe.result, result)) {
                if(recipe.result != null) arecipes.add(new CachedFormerRecipe(recipe));
            }
        }
    }

    @Override
    public void loadUsageRecipes(ItemStack ingredient)  {
        for(FormerRecipe recipe: FormerRecipeRegistry.altarRecipes) {
            if(NEIServerUtils.areStacksSameTypeCrafting(recipe.requiredItem, ingredient)) {
                if(recipe.result != null) arecipes.add(new CachedFormerRecipe(recipe));
            }
        }
    }

    @Override
    public void drawExtras(int id) {
        CachedFormerRecipe recipe = (CachedFormerRecipe) arecipes.get(id);
        Minecraft.getMinecraft().fontRenderer.drawString("\u00a77" + recipe.firstAspect + ": " + recipe.aspect1, 78, 5, 0);
        if (recipe.secondAspect != "" && recipe.secondAspect != " ") {
            Minecraft.getMinecraft().fontRenderer.drawString("\u00a77" + recipe.secondAspect + ": " + recipe.aspect2, 78, 15, 0);
        }

        if (recipe.thirdAspect != "" && recipe.thirdAspect != " ") {
            Minecraft.getMinecraft().fontRenderer.drawString("\u00a77" + recipe.thirdAspect + ": " + recipe.aspect3, 78, 25, 0);
        }
    }


    @Override
    public String getOverlayIdentifier() {
        return "formerrecipes";
    }

    @Override
    public void loadTransferRects() {
        transferRects.add(new RecipeTransferRect(new Rectangle(90, 32, 22, 16), "formerrecipes"));
    }

    @Override
    public String getRecipeName() {
        return "   " + StatCollector.translateToLocal("tile.carpetFormerFilled.name");
    }

    @Override
    public String getGuiTexture() {
        return new ResourceLocation("anchorman", "gui/nei/former.png").toString();
    }

    @Override
    public List<String> handleTooltip(GuiRecipe gui, List<String> currenttip, int id) {
        currenttip = super.handleTooltip(gui, currenttip, id);
        Point mouse = getMouse(getGuiWidth(gui), getGuiHeight(gui));
        CachedFormerRecipe recipe = (CachedFormerRecipe) arecipes.get(id);
        int yLow = id % 2 == 0 ? 38 : 102;
        int yHigh = id % 2 == 0 ? 72 : 136;
        if(mouse.x >= 19 && mouse.x <= 80 && mouse.y >= yLow && mouse.y <= yHigh) {
                if (!recipe.isMat) {
                    currenttip.add(EnumChatFormatting.DARK_RED + StatCollector.translateToLocal("former.template.needed"));
                } else {
                    currenttip.add(EnumChatFormatting.DARK_RED + StatCollector.translateToLocal("former.templateMat.needed"));
                }
            if (recipe.temporalThread) {
                currenttip.add(EnumChatFormatting.DARK_PURPLE + StatCollector.translateToLocal("former.temporalThread"));
            } else {
                currenttip.add(EnumChatFormatting.GRAY + StatCollector.translateToLocal("former.regularThread"));
            }
        }

        return currenttip;
    }

    //Mouse Position helper
    public Point getMouse(int width, int height) {
        Point mousepos = this.getMousePosition();
        int guiLeft = (width - 176) / 2;
        int guiTop = (height - 166) / 2;
        Point relMouse = new Point(mousepos.x - guiLeft, mousepos.y - guiTop);
        return relMouse;
    }

    //width helper, getting width normal way hates me on compile
    public int getGuiWidth(GuiRecipe gui) {
        try {
            Field f = gui.getClass().getField("width");
            return (Integer) f.get(gui);
        } catch (NoSuchFieldException e) {
            try {
                Field f = gui.getClass().getField("field_146294_l");
                return (Integer) f.get(gui);
            } catch (Exception e2) {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    //height helper, getting height normal way hates me on compile
    public int getGuiHeight(GuiRecipe gui) {
        try {
            Field f = gui.getClass().getField("height");
            return (Integer) f.get(gui);
        } catch (NoSuchFieldException e) {
            try {
                Field f = gui.getClass().getField("field_146295_m");
                return (Integer) f.get(gui);
            } catch (Exception e2) {
                return 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static Point getMousePosition() {
        Dimension size = displaySize();
        Dimension res = displayRes();
        return new Point(Mouse.getX() * size.width / res.width, size.height - Mouse.getY() * size.height / res.height - 1);
    }

    public static Dimension displaySize() {
        Minecraft mc = Minecraft.getMinecraft();
        ScaledResolution res = new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
        return new Dimension(res.getScaledWidth(), res.getScaledHeight());
    }

    public static Dimension displayRes() {
        Minecraft mc = Minecraft.getMinecraft();
        return new Dimension(mc.displayWidth, mc.displayHeight);
    }

}