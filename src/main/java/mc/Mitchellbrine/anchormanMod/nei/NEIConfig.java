package mc.Mitchellbrine.anchormanMod.nei;

import codechicken.nei.api.API;
import codechicken.nei.api.IConfigureNEI;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.item.ItemStack;

public class NEIConfig implements IConfigureNEI{

    @Override
    public void loadConfig() {
        API.registerRecipeHandler(new FormerRecipeHandler());
        API.hideItem(new ItemStack(MainMod.carpetFormerFilled));
        API.hideItem(new ItemStack(MainMod.carpetFormerFilledWool));
        API.hideItem(new ItemStack(MainMod.detectionCarpet1));
        API.hideItem(new ItemStack(MainMod.detectionCarpet2));
        API.hideItem(new ItemStack(MainMod.welcomeMatEast));
        API.hideItem(new ItemStack(MainMod.welcomeMatSouth));
        API.hideItem(new ItemStack(MainMod.welcomeMatWest));
    }

    @Override
    public String getName() {
        return "Weather Carpets NEI";
    }

    @Override
    public String getVersion() {
        return "1.0";
    }

}
