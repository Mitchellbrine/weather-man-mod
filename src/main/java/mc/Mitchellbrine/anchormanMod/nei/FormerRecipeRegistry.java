package mc.Mitchellbrine.anchormanMod.nei;

import net.minecraft.item.ItemStack;

import java.util.LinkedList;
import java.util.List;

public class FormerRecipeRegistry {

    public static List<FormerRecipe> altarRecipes = new LinkedList();

    public static void registerRecipe(ItemStack result, ItemStack requiredItem, String firstAspect, int aspect1, String secondAspect, int aspect2, String thirdAspect, int aspect3)
    {
        altarRecipes.add(new FormerRecipe(result, requiredItem, firstAspect, aspect1, secondAspect, aspect2, thirdAspect, aspect3));
    }

    public static void registerRecipe(ItemStack result, ItemStack requiredItem, String firstAspect, int aspect1, String secondAspect, int aspect2, String thirdAspect, int aspect3,boolean temporalThread)
    {
        altarRecipes.add(new FormerRecipe(result, requiredItem, firstAspect, aspect1, secondAspect, aspect2, thirdAspect, aspect3,temporalThread));
    }

    public static void registerRecipe(ItemStack result, ItemStack requiredItem, String firstAspect, int aspect1, String secondAspect, int aspect2, String thirdAspect, int aspect3,boolean temporalThread, boolean isMat)
    {
        altarRecipes.add(new FormerRecipe(result, requiredItem, firstAspect, aspect1, secondAspect, aspect2, thirdAspect, aspect3,temporalThread,isMat));
    }


}
