package mc.Mitchellbrine.anchormanMod.nei;

import net.minecraft.item.ItemStack;

public class FormerRecipe
{
    public int aspect1;
    public String aspectString1;
    public int aspect2;
    public String aspectString2;
    public int aspect3;
    public String aspectString3;
    public ItemStack requiredItem;
    public ItemStack result;
    public boolean temporalThread;
    public boolean isMat;

    public FormerRecipe(ItemStack result, ItemStack requiredItem, String firstAspect, int aspect1, String secondAspect, int aspect2, String thirdAspect, int aspect3)
    {
        this.result = result;
        this.requiredItem = requiredItem;
        this.aspectString1 = firstAspect;
        this.aspectString2 = secondAspect;
        this.aspectString3 = thirdAspect;
        this.aspect1 = aspect1;
        this.aspect2 = aspect2;
        this.aspect3 = aspect3;
        temporalThread = false;
        isMat = false;
    }

    public FormerRecipe(ItemStack result, ItemStack requiredItem, String firstAspect, int aspect1, String secondAspect, int aspect2, String thirdAspect, int aspect3, boolean temoralThread)
    {
        this.result = result;
        this.requiredItem = requiredItem;
        this.aspectString1 = firstAspect;
        this.aspectString2 = secondAspect;
        this.aspectString3 = thirdAspect;
        this.aspect1 = aspect1;
        this.aspect2 = aspect2;
        this.aspect3 = aspect3;
        temporalThread = temoralThread;
        isMat = false;
    }

    public FormerRecipe(ItemStack result, ItemStack requiredItem, String firstAspect, int aspect1, String secondAspect, int aspect2, String thirdAspect, int aspect3, boolean temoralThread, boolean isMat)
    {
        this.result = result;
        this.requiredItem = requiredItem;
        this.aspectString1 = firstAspect;
        this.aspectString2 = secondAspect;
        this.aspectString3 = thirdAspect;
        this.aspect1 = aspect1;
        this.aspect2 = aspect2;
        this.aspect3 = aspect3;
        temporalThread = temoralThread;
        this.isMat = isMat;
    }

    public ItemStack getResult()
    {
        return this.result;
    }

    public ItemStack getRequiredItem()
    {
        return this.requiredItem;
    }

    public String getAspectString1() {
        return this.aspectString1;
    }

    public String getAspectString2() {
        return this.aspectString2;
    }

    public String getAspectString3() {
        return this.aspectString3;
    }

    public int getAspect1() {
        return this.aspect1;
    }

    public int getAspect2() {
        return this.aspect2;
    }

    public int getAspect3(){
        return this.aspect3;
    }

    public boolean isTemporalThread() { return this.temporalThread; }

    public boolean isMat() { return this.isMat; }

}