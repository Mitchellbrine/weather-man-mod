package mc.Mitchellbrine.anchormanMod.util.config;

import cpw.mods.fml.client.config.GuiConfig;
import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;

public class WCConfigGUI extends GuiConfig {

    public WCConfigGUI(GuiScreen parent) {
        super(parent, new ConfigElement(MainMod.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(), "weatherManMod", false, false, GuiConfig.getAbridgedConfigPath(MainMod.config.toString()));
    }
}