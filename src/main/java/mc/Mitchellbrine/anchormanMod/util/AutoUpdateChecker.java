package mc.Mitchellbrine.anchormanMod.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import mc.Mitchellbrine.anchormanMod.common.core.MainMod;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class AutoUpdateChecker {

	
	public static boolean isUpdateAvailableStable(EntityPlayer player) throws IOException, MalformedURLException {
			BufferedReader versionFile = new BufferedReader(new InputStreamReader(new URL("https://dl.dropboxusercontent.com/u/133619815/WeatherCarpetsStable.txt").openStream()));
			String curVersion = versionFile.readLine();

			versionFile.close(); // YOU DONT NEED THE READER ANYMORE
		if (curVersion.contains(MainMod.lastStableVersion)) {
		return true;
		}
		else
		{
			player.addChatMessage(new ChatComponentText("["+ EnumChatFormatting.AQUA + "Weather Carpets" + EnumChatFormatting.WHITE + "] " + EnumChatFormatting.DARK_RED + "STABLE BUILD AVAILABLE! " + EnumChatFormatting.DARK_BLUE + "Check the repo for more info!"));
		}

		return false;
		}
	
	public static boolean isUpdateAvailableDev(EntityPlayer player) throws IOException, MalformedURLException {
		BufferedReader versionFile = new BufferedReader(new InputStreamReader(new URL("https://dl.dropboxusercontent.com/u/133619815/WeatherCarpetsDev.txt").openStream()));
		String curVersion = versionFile.readLine();

		if (curVersion.contains(MainMod.lastDevVersion)) {
			return true;
			}
			else
			{
				player.addChatMessage(new ChatComponentText("["+ EnumChatFormatting.AQUA + "Weather Carpets" + EnumChatFormatting.WHITE + "] " + EnumChatFormatting.DARK_RED + "DEVELOPMENT BUILD AVAILABLE! " + EnumChatFormatting.DARK_BLUE + "Check the " + EnumChatFormatting.DARK_BLUE +"repo for more info!" + EnumChatFormatting.GOLD + " Version Name: " + curVersion.toString()));
			}		
		
		versionFile.close(); // YOU DONT NEED THE READER ANYMORE


	return false;
	}
	
	public static boolean isUpdateAvailableBeta(EntityPlayer player) throws IOException, MalformedURLException {
		BufferedReader versionFile = new BufferedReader(new InputStreamReader(new URL("https://dl.dropboxusercontent.com/u/133619815/WeatherCarpetsBeta.txt").openStream()));
		String curVersion = versionFile.readLine();

		versionFile.close(); // YOU DONT NEED THE READER ANYMORE
	if (curVersion.contains(MainMod.lastBetaVersion)) {
	return true;
	}
	else
	{
		player.addChatMessage(new ChatComponentText("["+ EnumChatFormatting.AQUA + "Weather Carpets" + EnumChatFormatting.WHITE + "] " + EnumChatFormatting.DARK_RED + "PRE-RELEASE AVAILABLE! " + EnumChatFormatting.DARK_BLUE + "Check the repo for more info!" + EnumChatFormatting.GOLD + " Version Name: " + curVersion.toString()));
		
	}

	return false;
	}
	
}
