package mc.Mitchellbrine.anchormanMod.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import net.minecraft.entity.player.EntityPlayer;

public class CloudChecking {

	// User Registration Checking
	
	public static boolean userValidation(EntityPlayer player) throws IOException{

		BufferedReader userCloudFile = new BufferedReader(new InputStreamReader(new URL("https://dl.dropboxusercontent.com/u/133619815/BriniumModders%20Cloud/registeredUsers.txt").openStream()));
		String users = userCloudFile.readLine();
		
		if (users.contains(player.getCommandSenderName())) {
			return true;
		}
		
		userCloudFile.close();
		return false;
	}
	
	public static boolean isUserRegistered(EntityPlayer player) {
		try {
		if (userValidation(player) == true) {
			return true;
		}
		return false;
		}
		catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
}
